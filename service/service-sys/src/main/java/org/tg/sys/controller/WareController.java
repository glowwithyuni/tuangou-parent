package org.tg.sys.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.common.result.Result;
import org.tg.sys.service.WareService;

import javax.annotation.Resource;

/**
 * @Author Glow
 * @Date 2023-06-27 23:35:57
 * @Description 仓库Controller
 * @Version 1.0
 */
@Api(value = "仓库管理", tags = "仓库管理")
@RestController

@RequestMapping(value="/admin/sys/ware")
public class WareController {

    @Resource
    private WareService wareService;

    @ApiOperation(value = "获取全部仓库")
    @GetMapping("findAllList")
    public Result findAllList() {
        return Result.ok(wareService.list());
    }
}