package org.tg.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.sys.Region;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-27 23:32:13
 * @Description
 * @Version 1.0
 */
public interface RegionService  extends IService<Region> {
    //根据关键字获取地区列表
    List<Region> findRegionByKeyword(String keyword);
    //根据父ID获取省市区信息
    List<Region> findByParentId(Long parentId);
}
