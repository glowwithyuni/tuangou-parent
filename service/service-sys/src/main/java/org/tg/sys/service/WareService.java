package org.tg.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.sys.Ware;

/**
 * @Author Glow
 * @Date 2023-06-27 23:37:14
 * @Description
 * @Version 1.0
 */
public interface WareService extends IService<Ware> {
}
