package org.tg.sys.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.sys.Ware;
import org.tg.sys.mapper.WareMapper;
import org.tg.sys.service.WareService;

/**
 * @Author Glow
 * @Date 2023-06-27 23:38:37
 * @Description
 * @Version 1.0
 */
@Service
public class WareServiceImpl extends ServiceImpl<WareMapper, Ware>implements WareService {
}
