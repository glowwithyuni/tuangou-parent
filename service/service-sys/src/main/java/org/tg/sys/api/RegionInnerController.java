package org.tg.sys.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.common.result.Result;
import org.tg.model.product.Category;
import org.tg.model.sys.RegionWare;
import org.tg.sys.service.RegionWareService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-13 10:20:41
 * @Description
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/sys")
@Api("仓库区域调用接口服务")
public class RegionInnerController {
    @Autowired
    private RegionWareService regionWareService;
    @ApiOperation(value = "根据根据地域id获取仓库Id")
    @GetMapping("inner/getWareIdByRegionId/{regionId}")
    public Long getWareId(@PathVariable Long regionId) {
        return regionWareService.getWareIdByRegionId(regionId);
    }
    @ApiOperation(value = "获取所有区域信息")
    @GetMapping("/region/findAllList")
    public Result<List<RegionWare>> findAllList()
    {
        return Result.ok(regionWareService.list()) ;
    }

}
