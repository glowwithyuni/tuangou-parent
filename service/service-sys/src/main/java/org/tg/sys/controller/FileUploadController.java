package org.tg.sys.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tg.client.product.ProductFeignClient;
import org.tg.common.result.Result;

/**
 * @Author Glow
 * @Date 2023-07-17 14:46:24
 * @Description
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/sys/file")
public class FileUploadController {
    @Autowired
    private ProductFeignClient productFeignClient;

    @PostMapping("/fileUpload")
    private Result fileUpload(MultipartFile file) throws Exception{
        return Result.ok(productFeignClient.fileUpload(file).getData());
    }
}
