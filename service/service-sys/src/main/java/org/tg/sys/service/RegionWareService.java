package org.tg.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.sys.RegionWare;
import org.tg.vo.sys.RegionWareQueryVo;

/**
 * @Author Glow
 * @Date 2023-06-27 23:12:35
 * @Description 开通仓库的区域
 * @Version 1.0
 */
public interface RegionWareService extends IService<RegionWare> {

    //开通区域列表
    IPage<RegionWare> selectPage(Page<RegionWare> pageParam,
                                 RegionWareQueryVo regionWareQueryVo);
    //添加开通区域
    void saveRegionWare(RegionWare regionWare);
    //取消开通区域
    void updateStatus(Long id, Integer status);
    Long getWareIdByRegionId(Long regionId);
}