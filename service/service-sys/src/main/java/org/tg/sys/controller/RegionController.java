package org.tg.sys.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.tg.common.result.Result;
import org.tg.model.sys.Region;
import org.tg.sys.service.RegionService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-27 23:31:48
 * @Description 区域Controller
 * @Version 1.0
 */
@Api(tags = "区域接口")
@RestController
@RequestMapping("/admin/sys/region")

public class RegionController {

    @Resource
    private RegionService regionService;
    @ApiOperation("生成省市区区域地址")
    @GetMapping("findByParentId/{parentId}")
    public Result findByParentId(@PathVariable("parentId") Long parentId)
    {
        return Result.ok(regionService.findByParentId(parentId)) ;
    }
    @ApiOperation(value = "根据关键字获取地区列表")
    @GetMapping("findRegionByKeyword/{keyword}")
    public Result findSkuInfoByKeyword(@PathVariable("keyword") String keyword) {
        return Result.ok(regionService.findRegionByKeyword(keyword));
    }
    @ApiOperation(value = "根据ID返回省市区名")
    @GetMapping("/getRegionById/{id}")
    private Region getRegionById(@PathVariable("id") Long id)
    {
        return  regionService.getById(id);
    }
}