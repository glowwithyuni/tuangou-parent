package org.tg.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.sys.Region;

/**
 * @Author Glow
 * @Date 2023-06-27 23:34:26
 * @Description
 * @Version 1.0
 */
@Repository
public interface RegionMapper extends BaseMapper<Region> {

}
