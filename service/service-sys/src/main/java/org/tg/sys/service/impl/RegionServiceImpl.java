package org.tg.sys.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.sys.Region;
import org.tg.sys.mapper.RegionMapper;
import org.tg.sys.service.RegionService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-27 23:33:40
 * @Description
 * @Version 1.0
 */
@Service
public class RegionServiceImpl extends ServiceImpl<RegionMapper, Region> implements RegionService {
    //根据关键字获取地区列表
    @Override
    public List<Region> findRegionByKeyword(String keyword) {
        LambdaQueryWrapper<Region> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Region::getName, keyword);
        return baseMapper.selectList(queryWrapper);
    }

    @Override
    public List<Region> findByParentId(Long parentId) {
        LambdaQueryWrapper<Region> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Region::getParentId,parentId);
        return this.list(queryWrapper);
    }
}
