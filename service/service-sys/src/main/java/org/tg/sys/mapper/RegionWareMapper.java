package org.tg.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.sys.RegionWare;

/**
 * @Author Glow
 * @Date 2023-06-27 23:24:32
 * @Description 仓库区域mapper
 * @Version 1.0
 */
@Repository
public interface RegionWareMapper extends BaseMapper<RegionWare> {

}
