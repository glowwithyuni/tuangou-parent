package org.tg.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.sys.Ware;

/**
 * @Author Glow
 * @Date 2023-06-27 23:36:32
 * @Description 仓库Mapper
 * @Version 1.0
 */
@Repository
public interface WareMapper extends BaseMapper<Ware> {
}
