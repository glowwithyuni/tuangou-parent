package org.tg;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Glow
 * @Date 2023-06-28 23:18:52
 * @Description 商品信息启动类
 * @Version 1.0
 */
@SpringBootApplication
public class ServiceProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceProductApplication.class, args);
    }
}