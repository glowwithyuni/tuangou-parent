package org.tg.order.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tg.model.product.SkuPoster;
import org.tg.order.mapper.SkuPosterMapper;
import org.tg.order.service.SkuPosterService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:39:17
 * @Description
 * @Version 1.0
 */
@Service
public class SkuPosterServiceImpl extends ServiceImpl<SkuPosterMapper, SkuPoster> implements SkuPosterService {
    @Autowired
    private SkuPosterMapper skuPosterMapper;
    @Override
    public List<SkuPoster> findBySkuId(Long skuId) {
        LambdaQueryWrapper<SkuPoster> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SkuPoster::getSkuId,skuId);
        return skuPosterMapper.selectList(queryWrapper);
    }
}
