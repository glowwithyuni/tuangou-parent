package org.tg.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.SkuInfo;
import org.tg.vo.product.SkuInfoQueryVo;
import org.tg.vo.product.SkuInfoVo;
import org.tg.vo.product.SkuStockLockVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:07:35
 * @Description 商品规格信息接口
 * @Version 1.0
 */
public interface SkuInfoService extends IService<SkuInfo> {
    /**
     * 获取新人专享商品
     * @return
     */
    List<SkuInfo> findNewPersonList();
    //获取sku分页列表
    IPage<SkuInfo> selectPage(Page<SkuInfo> pageParam, SkuInfoQueryVo skuInfoQueryVo);
    //添加商品
    void saveSkuInfo(SkuInfoVo skuInfoVo);
    //获取商品
    SkuInfoVo getSkuInfoVo(Long id);
    //修改商品
    void updateSkuInfo(SkuInfoVo skuInfoVo);
    //商品审核
    void check(Long skuId, Integer status);
    //商品上架
    void publish(Long skuId, Integer status);
    //新人专享
    void isNewPerson(Long skuId, Integer status);
    //批量获取sku信息
    List<SkuInfo> findSkuInfoList(List<Long> skuIdList);
    void updateSkuType(List<Long> skuIdLists, Integer skuType);
    //根据关键字获取sku列表
    List<SkuInfo> findSkuInfoByKeyword(String keyword);

    /**
     * 锁定库存
     * @param skuStockLockVoList
     * @param orderToken
     * @return
     */
    Boolean checkAndLock(List<SkuStockLockVo> skuStockLockVoList,
                         String orderToken);
    void minusStock(String orderNo);
}
