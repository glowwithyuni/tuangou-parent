package org.tg.order.controller;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tg.common.result.Result;
import org.tg.order.service.FileUploadService;

/**
 * @Author Glow
 * @Date 2023-07-02 12:42:20
 * @Description OSS阿里云文件上传
 * @Version 1.0
 */
@Api(tags = "文件上传接口")
@RestController
@RequestMapping("admin/product")

public class FileUploadController {

    @Autowired
    private FileUploadService fileUploadService;

    //文件上传
    @PostMapping("fileUpload")
    public Result fileUpload(MultipartFile file) throws Exception{
        return Result.ok(fileUploadService.fileUpload(file));
    }
}