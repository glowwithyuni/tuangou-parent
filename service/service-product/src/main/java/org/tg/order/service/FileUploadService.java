package org.tg.order.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Author Glow
 * @Date 2023-07-02 12:43:01
 * @Description OSS阿里云文件上传接口
 * @Version 1.0
 */
public interface FileUploadService {

    //文件上传
    String fileUpload(MultipartFile file) throws Exception;
}