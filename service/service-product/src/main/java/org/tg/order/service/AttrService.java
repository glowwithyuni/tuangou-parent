package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.Attr;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-30 10:26:37
 * @Description 平台属性管理接口
 * @Version 1.0
 */
public interface AttrService extends IService<Attr> {

    //根据属性分组id 获取属性列表
    List<Attr> findByAttrGroupId(Long attrGroupId);
}
