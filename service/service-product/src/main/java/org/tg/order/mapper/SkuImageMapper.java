package org.tg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.SkuImage;

/**
 * @Author Glow
 * @Date 2023-07-02 11:42:31
 * @Description
 * @Version 1.0
 */
@Repository
public interface SkuImageMapper extends BaseMapper<SkuImage> {
}
