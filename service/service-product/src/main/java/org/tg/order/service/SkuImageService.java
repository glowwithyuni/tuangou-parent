package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.SkuImage;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:41:54
 * @Description
 * @Version 1.0
 */
public interface SkuImageService extends IService<SkuImage> {
    //根据skuId返回图片
    List<SkuImage> findBySkuId(Long skuId);
}
