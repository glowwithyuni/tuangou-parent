package org.tg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.SkuAttrValue;

/**
 * @Author Glow
 * @Date 2023-07-02 11:44:54
 * @Description
 * @Version 1.0
 */
@Repository
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {
}
