package org.tg.order.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tg.model.product.SkuImage;
import org.tg.order.mapper.SkuImageMapper;
import org.tg.order.service.SkuImageService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:42:15
 * @Description
 * @Version 1.0
 */
@Service
public class SkuImageServiceImpl extends ServiceImpl<SkuImageMapper, SkuImage> implements SkuImageService {
    @Autowired
    private SkuImageMapper skuImageMapper;

    @Override
    public List<SkuImage> findBySkuId(Long skuId) {
        LambdaQueryWrapper<SkuImage> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SkuImage::getSkuId,skuId);
        return skuImageMapper.selectList(queryWrapper);
    }
}
