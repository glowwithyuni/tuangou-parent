package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.Category;

/**
 * @Author Glow
 * @Date 2023-06-28 23:51:41
 * @Description 商品分类Mapper
 * @Version 1.0
 */
@Repository
public interface CategoryMapper  extends MPJBaseMapper<Category> {
}
