package org.tg.order.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.AttrGroup;
import org.tg.vo.product.AttrGroupQueryVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-30 10:19:29
 * @Description 属性分组服务接口
 * @Version 1.0
 */
public interface AttrGroupService extends IService<AttrGroup> {

    //平台属性分组列表
    IPage<AttrGroup> selectPage(Page<AttrGroup> pageParam, AttrGroupQueryVo attrGroupQueryVo);

    //查询所有属性分组
    List<AttrGroup> findAllList();
}