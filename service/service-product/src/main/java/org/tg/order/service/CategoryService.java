package org.tg.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.Category;
import org.tg.vo.product.CategoryQueryVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-28 23:50:10
 * @Description
 * @Version 1.0
 */
public interface CategoryService extends IService<Category> {

    //商品分类分页列表
    IPage<Category> selectPage(Page<Category> pageParam, CategoryQueryVo categoryQueryVo);
//    根据IDs查找商品分类集
    List<Category>findCategoryList(List<Long> categoryIdList);
    //查询所有商品分类
    List<Category> findAllList();
}