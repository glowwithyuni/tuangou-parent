package org.tg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.Attr;

/**
 * @Author Glow
 * @Date 2023-06-30 10:25:58
 * @Description
 * @Version 1.0
 */
@Repository
public interface AttrMapper extends BaseMapper<Attr> {
}
