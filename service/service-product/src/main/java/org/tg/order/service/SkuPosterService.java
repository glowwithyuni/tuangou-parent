package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.SkuPoster;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:38:36
 * @Description
 * @Version 1.0
 */
public interface SkuPosterService extends IService<SkuPoster> {
    //根据skuId返回商品文件路径
    List<SkuPoster> findBySkuId(Long skuId);
}
