package org.tg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.AttrGroup;

/**
 * @Author Glow
 * @Date 2023-06-30 10:20:35
 * @Description
 * @Version 1.0
 */
@Repository
public interface AttrGroupMapper  extends BaseMapper<AttrGroup> {
}
