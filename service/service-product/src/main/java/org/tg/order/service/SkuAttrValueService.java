package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.product.SkuAttrValue;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:44:34
 * @Description
 * @Version 1.0
 */
public interface SkuAttrValueService extends IService<SkuAttrValue> {
    /**
     *
     * @param skuId
     * @return
     * @description 返回商品属性
     */
    List<SkuAttrValue> findBySkuId(Long skuId);
}
