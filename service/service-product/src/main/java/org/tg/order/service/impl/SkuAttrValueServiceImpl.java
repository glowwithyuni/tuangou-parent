package org.tg.order.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tg.model.product.SkuAttrValue;
import org.tg.order.mapper.SkuAttrValueMapper;
import org.tg.order.service.SkuAttrValueService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-02 11:45:14
 * @Description
 * @Version 1.0
 */
@Service
public class SkuAttrValueServiceImpl extends ServiceImpl<SkuAttrValueMapper, SkuAttrValue> implements SkuAttrValueService {
    @Autowired
    private SkuAttrValueMapper skuAttrValueMapper;
    @Override
    public List<SkuAttrValue> findBySkuId(Long skuId) {
        LambdaQueryWrapper<SkuAttrValue> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SkuAttrValue::getSkuId,skuId);
        return skuAttrValueMapper.selectList(queryWrapper);
    }
}
