package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.product.SkuInfo;

/**
 * @Author Glow
 * @Date 2023-07-02 11:05:47
 * @Description
 * @Version 1.0
 */
@Repository
public interface SkuInfoMapper extends MPJBaseMapper<SkuInfo> {
}
