package org.tg.order.api;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tg.common.result.Result;
import org.tg.model.product.Category;
import org.tg.model.product.SkuInfo;
import org.tg.order.service.CategoryService;
import org.tg.order.service.FileUploadService;
import org.tg.order.service.SkuInfoService;
import org.tg.vo.product.SkuInfoVo;
import org.tg.vo.product.SkuStockLockVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-04 14:27:30
 * @Description 产品远程调用服务提供Controller
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/product")
@Api("商品远程调用接口服务")
public class ProductInnnerController {

    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SkuInfoService skuInfoService;
    //根据skuId获取sku信息
    @GetMapping("inner/getSkuInfoVo/{skuId}")
    public SkuInfoVo getSkuInfoVo(@PathVariable Long skuId) {
        return skuInfoService.getSkuInfoVo(skuId);
    }
    //根据CategoryId获取商品信息
    @GetMapping("inner/getSkuListByCategoryId/{categoryId}")
    public List<SkuInfo> getSkuListByCategoryId(@PathVariable("categoryId") Long categoryId)
    {
        LambdaQueryWrapper<SkuInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SkuInfo::getCategoryId,categoryId);
        return skuInfoService.list(queryWrapper);
    }
    @PostMapping("inner/fileUpload")
    public Result fileUpload(MultipartFile file) throws Exception{
        return Result.ok(fileUploadService.fileUpload(file));
    }
    @ApiOperation(value = "获取分类信息")
    @GetMapping("inner/findAllCategoryList")
    public List<Category> findAllCategoryList() {
        return categoryService.findAllList();
    }

    @ApiOperation(value = "获取新人专享")
    @GetMapping("inner/findNewPersonSkuInfoList")
    public List<SkuInfo> findNewPersonSkuInfoList() {
        return skuInfoService.findNewPersonList();
    }
    @ApiOperation(value = "根据分类id获取分类信息")
    @GetMapping("inner/getCategory/{categoryId}")
    public Category getCategory(@PathVariable Long categoryId) {
        return categoryService.getById(categoryId);
    }
    @ApiOperation(value = "根据skuId获取sku信息")
    @GetMapping("inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable("skuId") Long skuId) {
        return skuInfoService.getById(skuId);
    }


    @ApiOperation(value = "批量获取sku信息")
    @PostMapping("inner/findSkuInfoList")
    public List<SkuInfo> findSkuInfoList(@RequestBody List<Long> skuIdList) {
        return skuInfoService.findSkuInfoList(skuIdList);
    }

    @ApiOperation(value = "根据关键字获取sku列表")
    @GetMapping("inner/findSkuInfoByKeyword/{keyword}")
    public List<SkuInfo> findSkuInfoByKeyword(@PathVariable("keyword") String keyword) {
        return skuInfoService.findSkuInfoByKeyword(keyword);
    }
    @ApiOperation(value = "批量获取分类信息")
    @PostMapping("inner/findCategoryList")
    public List<Category> findCategoryList(@RequestBody List<Long> categoryIdList) {
        return categoryService.findCategoryList(categoryIdList);
    }
    @ApiOperation(value = "批量修改商品属性（普通/限时）")
    @PostMapping("inner/updateSkuType/{skuType}")
    public int updateSkuType(@RequestBody List<Long> skuIdLists, @PathVariable("skuType") Integer skuType) {
       skuInfoService.updateSkuType(skuIdLists,skuType);
       return 1;
    }

    @ApiOperation(value = "锁定库存")
    @PostMapping("inner/checkAndLock/{orderNo}")
    public Boolean checkAndLock(@RequestBody List<SkuStockLockVo> skuStockLockVoList, @PathVariable String orderNo) {
        return skuInfoService.checkAndLock(skuStockLockVoList, orderNo);
    }

}
