package org.tg.acl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Select;
import org.tg.model.acl.RolePermission;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 21:35:06
 * @Description
 * @Version 1.0
 */
public interface RolePermissionService extends IService<RolePermission> {

}
