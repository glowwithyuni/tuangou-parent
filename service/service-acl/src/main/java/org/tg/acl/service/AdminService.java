package org.tg.acl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.acl.Admin;
import org.tg.vo.acl.AdminQueryVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 12:03:12
 * @Description 用户接口
 * @Version 1.0
 */
public interface AdminService extends IService<Admin> {
    public Admin checkUserAndPassword(String username,String password);

    /**
     * 用户分页列表
     * @param pageParam
     * @param userQueryVo
     * @return
     */
    IPage<Admin> selectPage(Page<Admin> pageParam, AdminQueryVo userQueryVo);

}