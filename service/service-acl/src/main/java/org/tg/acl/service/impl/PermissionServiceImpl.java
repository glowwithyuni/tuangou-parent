package org.tg.acl.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tg.acl.helper.PermissionHelper;
import org.tg.acl.mapper.PermissionMapper;
import org.tg.acl.mapper.RolePermissionMapper;
import org.tg.acl.service.PermissionService;
import org.tg.acl.service.RolePermissionService;
import org.tg.model.acl.Permission;
import org.tg.model.acl.RolePermission;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @Author Glow
 * @Date 2023-06-24 15:48:36
 * @Description
 * @Version 1.0
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission>
        implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private RolePermissionService rolePermissionService;
    @Autowired
    private RolePermissionMapper rolePermissionMapper;
    //获取所有菜单
    @Override
    public List<Permission> queryAllMenu() {
        //获取全部权限数据
        List<Permission> allPermissionList = baseMapper.selectList(new QueryWrapper<Permission>().orderByAsc("CAST(id AS SIGNED)"));

        //把权限数据构建成树形结构数据
        List<Permission> result = PermissionHelper.bulid(allPermissionList);
        return result;
    }

    @Override
    public List<String> getPathByRoleIds(List<Long> roleIds) {
        Set<String> filterPath = Collections.newSetFromMap(new ConcurrentHashMap<>());
        List<Long> permissionIdlist = new ArrayList<>();
            for(Long roleId : roleIds)
            {
                permissionIdlist = rolePermissionMapper.getPermissionIdByRoleId(roleId);
                for(Long permissionId : permissionIdlist)
                {
                    //获取所有子级菜单路径，拿来鉴权用
                    getAllPathByRoleId(filterPath,permissionId,permissionIdlist);
                }

            }
            System.out.println("所有路径："+filterPath.toString());
        List<String> paths = filterPath.stream().collect(Collectors.toList());
        return  paths;

    }
    public String getAllPathByRoleId(Set<String> filterPath,Long permissionId,List<Long> permissionIdlist)
    {

            //先查询该权限的菜单路径
            Permission singlepermission = permissionMapper.getPathByPermissionId(permissionId);
        String [] singlespiltstr = singlepermission.getPath().split(",");
        for(String str : singlespiltstr)
        {
            filterPath.add(str);
        }

            //再查询该节点的子节点是否有权限列表
            List<Permission> permissionList = permissionMapper.getPathByPid(permissionId,permissionIdlist);
            if(permissionList == null)
            {
                //如果该权限下的子菜单为null，直接返回路径
                return "";
            }
            for(Permission  permission : permissionList)
            {
                String [] spiltstr = permission.getPath().split(",");
                for(String str : spiltstr)
                {
                    filterPath.add(str);
                }

                getAllPathByRoleId(filterPath,permission.getId(),permissionIdlist);

            }

        return "";
    }


    //递归删除菜单
    @Override
    public boolean removeChildById(Long id) {
        List<Long> idList = new ArrayList<>();
        this.selectChildListById(id, idList);
        idList.add(id);
        baseMapper.deleteBatchIds(idList);
        return true;
    }

    /**
     *	递归获取子节点
     * @param id
     * @param idList
     */
    private void selectChildListById(Long id, List<Long> idList) {
        List<Permission> childList = baseMapper.selectList(new QueryWrapper<Permission>().eq("pid", id).select("id"));
        childList.stream().forEach(item -> {
            idList.add(item.getId());
            this.selectChildListById(item.getId(), idList);
        });
    }
}
