package org.tg.acl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.acl.Permission;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 15:46:37
 * @Description 菜单服务接口
 * @Version 1.0
 */
public interface PermissionService extends IService<Permission> {

    //获取所有菜单列表
    List<Permission> queryAllMenu();
    //获取该用户ID下的请求路径
    List<String> getPathByRoleIds(List<Long> roleIds);
    //递归删除
    boolean removeChildById(Long id);
}