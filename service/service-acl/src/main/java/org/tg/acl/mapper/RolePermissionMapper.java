package org.tg.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.tg.model.acl.RolePermission;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 21:42:43
 * @Description
 * @Version 1.0
 */
@Repository
public interface RolePermissionMapper extends BaseMapper<RolePermission> {
    @Select("select permission_id from role_permission where role_id = #{roleId} and is_deleted = '0' order by permission_id")
    public List<Long> getPermissionIdByRoleId(Long roleId);
}
