package org.tg.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;
import org.tg.model.acl.Permission;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 15:45:46
 * @Description 菜单管理Mapper
 * @Version 1.0
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {
    //其实不用写的那么复杂，通过rolepermission根据Id搜索path路径就行了，我想太多了hh
    @Select("<script> SELECT v2.id,v2.path FROM `permission` v1, `permission` v2 " +
            "where v1.id=#{id}" +
            " and v2.pid = v1.id" +
            "  and v2.id " +
            "in <foreach item='item' index='index' collection='permissionIdList'  \n" +
            "         open='(' separator=',' close=')'> #{item} </foreach> and v1.is_deleted = '0' and v2.is_deleted = '0' </script>")
     List<Permission> getPathByPid(@Param("id")Long id,@Param("permissionIdList") List<Long> permissionIdList);
    @Select("SELECT path from `permission` where id = #{permissionId} and is_deleted = '0' ")
    Permission getPathByPermissionId(Long permissionId);
}