package org.tg.acl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.user.Leader;

/**
 * @Author Glow
 * @Date 2023-07-11 14:26:17
 * @Description 团长
 * @Version 1.0
 */
@Repository
public interface LeaderMapper extends BaseMapper<Leader> {
}
