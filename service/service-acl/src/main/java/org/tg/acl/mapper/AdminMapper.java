package org.tg.acl.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.acl.Admin;

/**
 * @Author Glow
 * @Date 2023-06-24 12:04:50
 * @Description
 * @Version 1.0
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {

}