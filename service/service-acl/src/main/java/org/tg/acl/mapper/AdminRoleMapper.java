package org.tg.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.acl.AdminRole;

/**
 * @Author Glow
 * @Date 2023-06-24 13:02:52
 * @Description
 * @Version 1.0
 */
@Repository
public interface AdminRoleMapper extends BaseMapper<AdminRole> {

}
