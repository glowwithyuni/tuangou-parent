package org.tg.acl.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tg.acl.mapper.AdminMapper;
import org.tg.acl.service.AdminRoleService;
import org.tg.acl.service.AdminService;
import org.tg.acl.service.RoleService;
import org.tg.model.acl.Admin;
import org.tg.model.acl.AdminRole;
import org.tg.vo.acl.AdminQueryVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 12:03:53
 * @Description
 * @Version 1.0
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Autowired
    private AdminMapper userMapper;
    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private RoleService roleService;

    @Override
    public Admin checkUserAndPassword(String username, String password) {
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Admin::getUsername,username);
        queryWrapper.eq(Admin::getPassword,password);
        return this.getOne(queryWrapper);
    }



    @Override
    public IPage<Admin> selectPage(Page<Admin> pageParam, AdminQueryVo userQueryVo) {
        //获取用户名称条件值
        String name = userQueryVo.getName();
        //创建条件构造器
        LambdaQueryWrapper<Admin> wrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(name)) {
            //封装条件
            wrapper.like(Admin::getName,name);
        }
        //调用mapper方法
        IPage<Admin> pageModel = baseMapper.selectPage(pageParam,wrapper);
        return pageModel;
    }
}