package org.tg.acl.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.acl.Role;
import org.tg.vo.acl.RoleQueryVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-06-24 11:17:03
 * @Description 权限接口
 * @Version 1.0
 */
public interface RoleService extends IService<Role> {

    //角色分页列表
    IPage<Role> selectPage(Page<Role> pageParam, RoleQueryVo roleQueryVo);
    /**
     * 分配角色
     * @param adminId
     * @param roleIds
     */
    void saveUserRoleRealtionShip(Long adminId, Long[] roleIds);

    /**
     * 根据用户获取角色数据
     * @param adminId
     * @return
     */
    Map<String, Object> findRoleByUserId(Long adminId);
    /**
     * 分配角色菜单
     * @param roleId
     * @param permissionIds
     */
    void saveRolePermissionRealtionShip(Long roleId, List<Long> permissionIds);

    /**
     * 根据角色获取菜单数据
     * @param roleId
     * @return
     */
    Map<String, Object> findPermissionByRole(Long roleId);
}