package org.tg.acl.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tg.acl.service.RolePermissionService;
import org.tg.acl.service.RoleService;
import org.tg.common.result.Result;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-06-25 15:09:28
 * @Description 权限菜单Controller
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/acl/permission")
@Api(tags = "角色菜单权限管理")
@Slf4j

public class    RolePermissionController {

    @Autowired
    private RoleService roleService;
    @ApiOperation(value = "根据角色获取菜单数据")
    @GetMapping("/toAssign/{roleId}")
    public Result toAssignPermission(@PathVariable Long roleId) {
        Map<String, Object> permissionMap = roleService.findPermissionByRole(roleId);
//        System.out.println(permissionMap.toString());
        return Result.ok(permissionMap);
    }

    @ApiOperation(value = "根据角色分配菜单")
    @GetMapping("/doAssign")
    public Result doAssignPermission(@RequestParam Long roleId, @RequestParam List<Long> permissionId) {

        roleService.saveRolePermissionRealtionShip(roleId,permissionId);
        return Result.ok();
    }
}
