package org.tg.acl.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.acl.mapper.RolePermissionMapper;
import org.tg.acl.service.RolePermissionService;
import org.tg.model.acl.RolePermission;

/**
 * @Author Glow
 * @Date 2023-06-24 22:21:26
 * @Description
 * @Version 1.0
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {
}
