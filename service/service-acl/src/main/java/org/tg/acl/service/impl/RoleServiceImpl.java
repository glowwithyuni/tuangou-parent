package org.tg.acl.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tg.acl.helper.PermissionHelper;
import org.tg.acl.mapper.RoleMapper;
import org.tg.acl.service.AdminRoleService;
import org.tg.acl.service.PermissionService;
import org.tg.acl.service.RolePermissionService;
import org.tg.acl.service.RoleService;
import org.tg.common.auth.AuthContextHolder;
import org.tg.common.constant.RedisConst;
import org.tg.model.acl.AdminRole;
import org.tg.model.acl.Permission;
import org.tg.model.acl.Role;
import org.tg.model.acl.RolePermission;
import org.tg.vo.acl.AdminLoginVo;
import org.tg.vo.acl.RoleQueryVo;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Author Glow
 * @Date 2023-06-24 11:17:43
 * @Description
 * @Version 1.0
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role>
        implements RoleService {
    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private RolePermissionService rolePermissionService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisTemplate redisTemplate;
    //角色分页列表
    @Override
    public IPage<Role> selectPage(Page<Role> pageParam, RoleQueryVo roleQueryVo) {
        //获取条件值：角色名称
        String roleName = roleQueryVo.getRoleName();
        //创建条件构造器对象
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        //判断条件值是否为空
        if(!StringUtils.isEmpty(roleName)) {
            //封装条件
            wrapper.like(Role::getRoleName,roleName);
        }
        //调用mapper方法实现条件分页查询
        IPage<Role> pageModel = baseMapper.selectPage(pageParam, wrapper);
        return pageModel;
    }

    /**
     * 分配角色
     * @param adminId
     * @param roleIds
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveUserRoleRealtionShip(Long adminId, Long[] roleIds) {
        //删除用户分配的角色数据
        adminRoleService.remove(new QueryWrapper<AdminRole>().eq("admin_id", adminId));

        //分配新的角色
        List<AdminRole> userRoleList = new ArrayList<>();
        for(Long roleId : roleIds) {
            if(StringUtils.isEmpty(String.valueOf(roleId)))
            {
                continue;
            }
            AdminRole userRole = new AdminRole();
            userRole.setAdminId(adminId);
            userRole.setRoleId(roleId);
            userRoleList.add(userRole);
        }
        adminRoleService.saveBatch(userRoleList);
        AdminLoginVo adminLoginVo = (AdminLoginVo) redisTemplate.opsForValue().get
                (RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId);

        //更新后，需要同步redis的信息
        if(adminLoginVo !=null)
        {
            if(AuthContextHolder.getAdminId() == adminId)
            {
                List<Long> roleList = new ArrayList<>(Arrays.asList(roleIds));
                adminLoginVo.setRoleId(roleList);
                //登录的role信息更新
//            redisTemplate.opsForValue().set(RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId,adminLoginVo);
                redisTemplate.opsForValue().set
                        (RedisConst.ADMIN_LOGIN_KEY_PREFIX+AuthContextHolder.getAdminId(),
                                adminLoginVo,
                                RedisConst.ADMIN_LOGIN_KEY_TIMEOUT,
                                TimeUnit.MINUTES);
                //获取权限列表
                List<String> path = permissionService.getPathByRoleIds(roleList);
                //存储权限列表
//            redisTemplate.opsForList().leftPop(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId);
//            redisTemplate.opsForList().leftPushAll(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId,path);
                redisTemplate.opsForValue().set
                        (RedisConst.ADMIN_ROLE_PATH_PREFIX+AuthContextHolder.getAdminId(),
                                path,
                                RedisConst.ADMIN_ROLE_PATH_TIMEOUT,
                                TimeUnit.MINUTES);
            }
            //感觉有点废话文学了，算了算了功能正常就行
            else {
                List<Long> roleList = new ArrayList<>(Arrays.asList(roleIds));
                adminLoginVo.setRoleId(roleList);
                //登录的role信息更新
//            redisTemplate.opsForValue().set(RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId,adminLoginVo);
                redisTemplate.opsForValue().set
                        (RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId,
                                adminLoginVo,
                                RedisConst.ADMIN_LOGIN_KEY_TIMEOUT,
                                TimeUnit.MINUTES);
                //获取权限列表
                List<String> path = permissionService.getPathByRoleIds(roleList);
                //存储权限列表
//            redisTemplate.opsForList().leftPop(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId);
//            redisTemplate.opsForList().leftPushAll(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId,path);
                redisTemplate.opsForValue().set
                        (RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId,
                                path,
                                RedisConst.ADMIN_ROLE_PATH_TIMEOUT,
                                TimeUnit.MINUTES);
            }
        }



    }

    /**
     * 根据用户获取角色数据
     * @param adminId
     * @return
     */
    @Override
    public Map<String, Object> findRoleByUserId(Long adminId) {
        //查询所有的角色
        List<Role> allRolesList =baseMapper.selectList(null);

        //拥有的角色id
        List<AdminRole> existUserRoleList = adminRoleService.list(new QueryWrapper<AdminRole>().eq("admin_id", adminId).select("role_id"));
        List<Long> existRoleList = existUserRoleList.stream().map(c->c.getRoleId()).collect(Collectors.toList());

        //对角色进行分类
        List<Role> assignRoles = new ArrayList<Role>();
        for (Role role : allRolesList) {
            //已分配
            if(existRoleList.contains(role.getId())) {
                assignRoles.add(role);
            }
        }

        Map<String, Object> roleMap = new HashMap<>();
        roleMap.put("assignRoles", assignRoles);
        roleMap.put("allRolesList", allRolesList);
        return roleMap;
    }

    @Override
    public void saveRolePermissionRealtionShip(Long roleId, List<Long> permissionIds) {
        //删除角色分配的菜单数据
        rolePermissionService.remove(new QueryWrapper<RolePermission>().eq("role_id", roleId));

        //分配新的角色
        List<RolePermission> rolePermissionList = new ArrayList<>();
        for(Long permissionId : permissionIds) {
            if(StringUtils.isEmpty(String.valueOf(permissionId)))
            {
                continue;
            }
            RolePermission rolePermission = new RolePermission();
            rolePermission.setPermissionId(permissionId);
            rolePermission.setRoleId(roleId);
            rolePermissionList.add(rolePermission);
        }
        rolePermissionService.saveBatch(rolePermissionList);
        //更改后查看是否是修改了当前用户所拥有的角色，如果是则修改
        AdminLoginVo adminLoginVo = (AdminLoginVo) redisTemplate.opsForValue().get(RedisConst.ADMIN_LOGIN_KEY_PREFIX+AuthContextHolder.getAdminId());
        if(adminLoginVo !=null)
        {
            if(adminLoginVo.getRoleId().contains(roleId))
            {
                //将redis存储该角色的Path进行刷新
                List<String> path = permissionService.getPathByRoleIds(adminLoginVo.getRoleId());
//        adminLoginVo.setPath(path);
                //存储权限列表
                redisTemplate.opsForValue().set
                        (RedisConst.ADMIN_ROLE_PATH_PREFIX+AuthContextHolder.getAdminId(),
                                path,
                                RedisConst.ADMIN_ROLE_PATH_TIMEOUT,
                                TimeUnit.MINUTES);
//     redisTemplate.psFoorList().leftPushAll(RedisConst.ADMIN_ROLE_PATH_PREFIX+AuthContextHolder.getAdminId(),path);

            }
        }
    }

    @Override
    public Map<String, Object> findPermissionByRole(Long roleId) {
        //1 查询所有权限
        List<Permission> AllPermissions = permissionService.list();
List<Permission> allPermissions = PermissionHelper.bulid(AllPermissions);
        //2 根据角色id查询角色分配所有权限
        //2.1 根据角色id查询 Role_Permission 表， 查询角色分配权限表
        LambdaQueryWrapper<RolePermission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RolePermission::getRoleId, roleId);
        List<RolePermission> rolePermissions = rolePermissionService.list(wrapper);
        //2.2 根据上一步结果，获取角色分配的所有权限表
        List<Long> permissionIds = rolePermissions.stream().map(item -> item.getPermissionId()).collect(Collectors.toList());
        Map<String, Object> result = new HashMap<>();
        result.put("allPermissionList", allPermissions);
        result.put("Ids", permissionIds);


        return result;

    }

}