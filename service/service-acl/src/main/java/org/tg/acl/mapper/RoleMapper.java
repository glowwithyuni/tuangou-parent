package org.tg.acl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.acl.Role;

/**
 * @Author Glow
 * @Date 2023-06-24 11:21:04
 * @Description 权限mapper
 * @Version 1.0
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}