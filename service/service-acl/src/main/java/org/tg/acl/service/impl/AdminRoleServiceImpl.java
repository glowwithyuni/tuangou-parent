package org.tg.acl.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.acl.mapper.AdminRoleMapper;
import org.tg.acl.service.AdminRoleService;
import org.tg.model.acl.AdminRole;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 13:02:27
 * @Description 用户角色服务实现类
 * @Version 1.0
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole>
        implements AdminRoleService {
    @Override
    public List<Long> getRoleIdListByAdminId(Long adminId) {
        LambdaQueryWrapper<AdminRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminRole::getAdminId,adminId);
        List<AdminRole> adminRoleList = this.list(queryWrapper);
        List<Long> list = new ArrayList<>();
        for(AdminRole adminRole: adminRoleList)
        {
            list.add(adminRole.getRoleId());
        }
        return list;

    }

    @Override
    public void removeByAdminId(Long adminId) {
        LambdaQueryWrapper<AdminRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AdminRole::getAdminId,adminId);
        this.remove(queryWrapper);
    }

    @Override
    public void removeBatchByAdminIdList(List<Long> adminIdList) {
        LambdaQueryWrapper<AdminRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(AdminRole::getAdminId,adminIdList);
        this.remove(queryWrapper);
    }
}