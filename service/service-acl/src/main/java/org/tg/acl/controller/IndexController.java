package org.tg.acl.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.tg.acl.service.AdminRoleService;
import org.tg.acl.service.AdminService;
import org.tg.acl.service.PermissionService;
import org.tg.common.auth.AuthContextHolder;
import org.tg.common.constant.RedisConst;
import org.tg.common.result.Result;
import org.tg.common.util.MD5;
import org.tg.common.util.helper.JwtHelper;
import org.tg.model.acl.Admin;
import org.tg.vo.acl.AdminLoginVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author Glow
 * @Date 2023-06-24 10:44:25
 * @Description 登录Controller
 * @Version 1.0
 */
@RestController
@RequestMapping("/admin/acl/index")
public class IndexController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminRoleService adminRoleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 1、请求登陆的login
     */
    @PostMapping("login")
    public Result login(@RequestBody Map<String,Object> loginmaps) {

        System.out.println(loginmaps);
        String username = (String)loginmaps.get("username");
        String password = (String)loginmaps.get("password");
        Map<String,Object> map = new HashMap<>();
        String encryptPassword = MD5.encrypt(password);
        System.out.println(encryptPassword);
        Admin admin = adminService.checkUserAndPassword(username,encryptPassword);
        Long adminId = admin.getId();
        String adminName =admin.getUsername();
        Long wareId =admin.getWareId();
        List<Long> roleId = adminRoleService.getRoleIdListByAdminId(adminId);
        String token = JwtHelper.createAdminToken(adminId,wareId);
        AdminLoginVo adminLoginVo = new AdminLoginVo();
        adminLoginVo.setAdminId(adminId);
        adminLoginVo.setName(adminName);
        adminLoginVo.setWareId(wareId);
        adminLoginVo.setRoleId(roleId);

        //获取权限列表
        List<String> path = permissionService.getPathByRoleIds(roleId);
//        adminLoginVo.setPath(path);
        //存储登录信息和权限列表
//        redisTemplate.opsForList().leftPop(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId);
//        redisTemplate.opsForList().leftPushAll(RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId,path);
      redisTemplate.opsForValue().set
              (RedisConst.ADMIN_ROLE_PATH_PREFIX+adminId,
                      path,
                      RedisConst.ADMIN_ROLE_PATH_TIMEOUT,
                      TimeUnit.MINUTES);
        redisTemplate.opsForValue().set
                (RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId,
                        adminLoginVo,
                        RedisConst.ADMIN_LOGIN_KEY_TIMEOUT,
                        TimeUnit.MINUTES);
        map.put("token",token);
        map.put("usertype","adminuser");
        if(admin == null)
        {
            return Result.fail();
        }
//        map.put("token","token-admin");

        return Result.ok(map);
    }
    /**
     * 2 获取用户信息
     */
    @GetMapping("info")
    public Result info(){
        Long adminId = AuthContextHolder.getAdminId();
AdminLoginVo adminLoginVo = (AdminLoginVo) redisTemplate.opsForValue().get(RedisConst.ADMIN_LOGIN_KEY_PREFIX+adminId);
      System.out.println(adminLoginVo);
        Map<String,Object> map = new HashMap<>();
        map.put("name",adminLoginVo.getName());
        map.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return Result.ok(map);
    }

    /**
     * 3 退出
     */
    @PostMapping("logout")
    public Result logout(){
        return Result.ok("logout successful");
    }
}