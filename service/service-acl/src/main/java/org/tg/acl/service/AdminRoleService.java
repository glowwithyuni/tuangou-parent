package org.tg.acl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.acl.Admin;
import org.tg.model.acl.AdminRole;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-24 12:57:17
 * @Description 用户角色接口
 * @Version 1.0
 */
public interface AdminRoleService extends IService<AdminRole> {

    public List<Long> getRoleIdListByAdminId(Long adminId);
    public void removeByAdminId(Long adminId);
    public void removeBatchByAdminIdList(List<Long> adminIdList);
}