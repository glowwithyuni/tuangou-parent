package org.tg.cart.service;

import org.tg.model.order.CartInfo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-18 22:33:36
 * @Description
 * @Version 1.0
 */
public interface CartInfoService {
    //购物车列表
    List<CartInfo> getCartList(Long userId);

    // 添加购物车 用户Id，商品Id，商品数量。
    void addToCart(Long skuId, Long userId, Integer skuNum);

    void deleteCart(Long skuId, Long userId);

    /**
     * 批量删除购物车
     * @param userId
     */
    void deleteAllCart(Long userId);

    void batchDeleteCart(List<Long> skuIdList, Long userId);





    /**
     * 更新选中状态
     *
     * @param userId
     * @param isChecked
     * @param skuId
     */
    void checkCart(Long userId, Integer isChecked, Long skuId);

    void checkAllCart(Long userId, Integer isChecked);

    void batchCheckCart(List<Long> skuIdList, Long userId, Integer isChecked);
    //获取当前用户购物车选中购物项
    List<CartInfo> getCartCheckedList(Long userId);
    //根据userId删除选中购物车记录
    void deleteCartChecked(Long userId);
}
