package org.tg.home.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.client.product.ProductFeignClient;
import org.tg.common.result.Result;

/**
 * @Author Glow
 * @Date 2023-07-18 22:52:36
 * @Description 小程序端商品分类
 * @Version 1.0
 */
@Api(tags = "商品分类")
@RestController
@RequestMapping("api/home")
public class CategoryApiController {
    @Autowired
    private ProductFeignClient productFeignClient;
    @ApiOperation(value = "获取分类信息")
    @GetMapping("category")
    public Result index() {
        return Result.ok(productFeignClient.findAllCategoryList());
    }
}
