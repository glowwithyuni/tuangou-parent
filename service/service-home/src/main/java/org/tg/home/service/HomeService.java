package org.tg.home.service;

import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-15 14:56:40
 * @Description
 * @Version 1.0
 */
public interface HomeService {

    //首页数据
    Map<String, Object> home(Long userId);
}
