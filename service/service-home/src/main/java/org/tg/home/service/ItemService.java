package org.tg.home.service;

import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-21 11:31:13
 * @Description
 * @Version 1.0
 */
public interface ItemService {
    //获取sku详细信息
    Map<String, Object> item(Long skuId, Long userId);
}
