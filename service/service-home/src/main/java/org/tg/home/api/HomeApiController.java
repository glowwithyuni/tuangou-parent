package org.tg.home.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.common.auth.AuthContextHolder;
import org.tg.common.result.Result;
import org.tg.home.service.HomeService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @Author Glow
 * @Date 2023-07-15 14:55:58
 * @Description 小程序首页接口
 * @Version 1.0
 */
@Api(tags = "首页接口")
@RestController
@RequestMapping("api/home")
public class HomeApiController {

    @Autowired
    private HomeService homeService;

    @ApiOperation(value = "获取首页数据")
    @GetMapping("index")
    public Result index(HttpServletRequest request) {
        // 获取用户Id
        Long userId = AuthContextHolder.getUserId();
        return Result.ok(homeService.home(userId));
    }
}