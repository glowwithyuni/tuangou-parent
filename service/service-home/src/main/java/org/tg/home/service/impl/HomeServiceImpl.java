package org.tg.home.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.tg.client.SearchFeignClient;
import org.tg.client.activity.ActivityFeignClient;
import org.tg.client.seckill.SeckillFeignClient;
import org.tg.client.user.UserFeignClient;
import org.tg.client.product.ProductFeignClient;
import org.tg.home.service.HomeService;
import org.tg.model.product.Category;
import org.tg.model.product.SkuInfo;
import org.tg.model.search.SkuEs;
import org.tg.model.user.Leader;
import org.tg.model.user.UserDelivery;
import org.tg.vo.user.LeaderAddressVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author Glow
 * @Date 2023-07-15 14:57:01
 * @Description
 * @Version 1.0
 */
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class HomeServiceImpl implements HomeService {

    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private SeckillFeignClient seckillFeignClient;

    @Autowired
    private SearchFeignClient searchFeignClient;

    @Autowired
    private UserFeignClient userFeignClient;

    @Autowired
    private ActivityFeignClient activityFeignClient;

    @Override
    public Map<String, Object> home(Long userId) {

        Map<String, Object> result = new HashMap<>();

        //获取分类信息
        List<Category> categoryList = productFeignClient.findAllCategoryList();
        result.put("categoryList", categoryList);

        //获取新人专享商品
        List<SkuInfo> newPersonSkuInfoList =  productFeignClient.findNewPersonSkuInfoList();
        result.put("newPersonSkuInfoList", newPersonSkuInfoList);

        //获取用户首页秒杀数据
        Map<String,Object> seckillmap =   seckillFeignClient.findHomeData();
        result.put("seckillList",seckillmap);
        //提货点地址信息
//        LeaderAddressVo leaderAddressVo = userFeignClient.getLeaderAddressVoByUserId(userId);


        //获取爆品商品
        List<SkuEs> hotSkuList = searchFeignClient.findHotSkuList();
        //获取sku对应的促销活动标签
        if(!CollectionUtils.isEmpty(hotSkuList)) {
            List<Long> skuIdList = hotSkuList.stream().map(sku -> sku.getId()).collect(Collectors.toList());
            Map<Long, List<String>> skuIdToRuleListMap = activityFeignClient.findActivity(skuIdList);
            if(null != skuIdToRuleListMap) {
                hotSkuList.forEach(skuEs -> {
                    skuEs.setRuleList(skuIdToRuleListMap.get(skuEs.getId()));
                });
            }
        }
        result.put("hotSkuList", hotSkuList);
        //添加用户选定的提货点信息
        try{



            Long leaderId = userFeignClient.getLeaderIdByUserId(userId);
            LeaderAddressVo leaderAddressVo = new LeaderAddressVo();
            Leader leader = new Leader();
            leader = userFeignClient.getLeaderById(leaderId);
            leaderAddressVo.setUserId(userId);
            leaderAddressVo.setLeaderId(leaderId);
            leaderAddressVo.setLeaderName(leader.getName());
            leaderAddressVo.setLeaderPhone(leader.getPhone());
            UserDelivery userDelivery = userFeignClient.getUserDeliveryByUserIdAndLeaderId(userId,leaderId);
            if(!(userDelivery == null))
            {
                leaderAddressVo.setWareId(userDelivery.getWareId());
            }

            leaderAddressVo.setTakeName(leader.getTakeName());
            leaderAddressVo.setProvince(String.valueOf(leader.getProvince()));
            leaderAddressVo.setCity(String.valueOf(leader.getCity()));
            leaderAddressVo.setDistrict(String.valueOf(leader.getDistrict()));
            leaderAddressVo.setDetailAddress(leader.getDetailAddress());
            leaderAddressVo.setLongitude(String.valueOf(leader.getLongitude()));
            leaderAddressVo.setLatitude(String.valueOf(leader.getLatitude()));
            leaderAddressVo.setStorePath(leader.getStorePath());
            result.put("leaderAddressVo", leaderAddressVo);
        }catch (Exception e)
        {
            result.put("leaderAddressVo",null);
        }finally {
            return result;
        }


    }
}