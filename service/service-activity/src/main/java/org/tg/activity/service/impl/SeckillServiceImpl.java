package org.tg.activity.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.tg.activity.mapper.SeckillMapper;
import org.tg.activity.service.SeckillService;
import org.tg.model.activity.Seckill;
import org.tg.vo.activity.SeckillQueryVo;

import javax.annotation.Resource;

/**
 * @Author Glow
 * @Date 2023-07-08 14:14:25
 * @Description
 * @Version 1.0
 */
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillServiceImpl extends ServiceImpl<SeckillMapper, Seckill> implements SeckillService {

    @Resource
    private SeckillMapper seckillMapper;

    @Override
    public IPage<Seckill> selectPage(Page<Seckill> pageParam, SeckillQueryVo seckillQueryVo) {
        Integer status = seckillQueryVo.getStatus();
        String title = seckillQueryVo.getTitle();
        LambdaQueryWrapper<Seckill> wrapper = new LambdaQueryWrapper<>();
        if(status != null) {
            wrapper.eq(Seckill::getStatus,status);
        }
        if(!StringUtils.isEmpty(title)) {
            wrapper.like(Seckill::getTitle,title);
        }
        IPage<Seckill> seckillPage = baseMapper.selectPage(pageParam, wrapper);
        return seckillPage;
    }

    @Override
    public void updateStatus(Long id, Integer status) {
        Seckill seckill = new Seckill();
        seckill.setStatus(status);
        seckill.setId(id);
        this.updateById(seckill);
    }

}