package org.tg.activity.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import jdk.nashorn.internal.objects.Global;
import org.joda.time.DateTime;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.tg.activity.mapper.SeckillSkuMapper;
import org.tg.activity.service.SeckillSkuService;
import org.tg.client.product.ProductFeignClient;
import org.tg.common.constant.RedisConst;
import org.tg.common.exception.CustomExceptionHandler;
import org.tg.common.exception.GlobalExceptionHandler;
import org.tg.common.result.ResultCodeEnum;
import org.tg.common.service.RabbitService;
import org.tg.common.util.DateUtil;
import org.tg.enums.SkuType;
import org.tg.model.activity.Seckill;
import org.tg.model.activity.SeckillSku;
import org.tg.model.activity.SeckillTime;
import org.tg.model.product.SkuInfo;
import org.tg.vo.activity.SeckillSkuQueryVo;
import org.tg.vo.activity.SeckillSkuVo;
import org.tg.vo.product.SkuStockLockVo;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author Glow
 * @Date 2023-07-08 14:16:52
 * @Description
 * @Version 1.0
 */
@Service
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillSkuServiceImpl extends ServiceImpl<SeckillSkuMapper, SeckillSku> implements SeckillSkuService {

    @Autowired
    private SeckillSkuMapper seckillSkuMapper;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private ProductFeignClient productFeignClient;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public IPage<SeckillSku> selectPage(Page<SeckillSku> pageParam, SeckillSkuQueryVo seckillSkuQueryVo) {
        LambdaQueryWrapper<SeckillSku> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SeckillSku::getSeckillId,seckillSkuQueryVo.getSeckillId());
        queryWrapper.eq(SeckillSku::getSeckillTimeId,seckillSkuQueryVo.getSeckillTimeId());
        IPage<SeckillSku> seckillSkuIPage = seckillSkuMapper.selectPage(pageParam, queryWrapper);
        if(CollectionUtils.isEmpty(seckillSkuIPage.getRecords())) {
            return seckillSkuIPage;
        }

        List<Long> skuIdList = seckillSkuIPage.getRecords().stream().map(SeckillSku::getSkuId).collect(Collectors.toList());
        List<SkuInfo> skuInfoList = productFeignClient.findSkuInfoList(skuIdList);
        Map<Long, SkuInfo> skuIdToSkuInfoMap = skuInfoList.stream().collect(Collectors.toMap(SkuInfo::getId, SkuInfo -> SkuInfo));
        seckillSkuIPage.getRecords().stream().forEach(item -> {
            item.setSkuInfo(skuIdToSkuInfoMap.get(item.getSkuId()));
        });
        return seckillSkuIPage;
    }

    @Override
    public void save(List<SeckillSku> seckillSkuList) {
        this.saveBatch(seckillSkuList);

        //更新sku为秒杀商品
        List<Long> skuIdList = seckillSkuList.stream().map(SeckillSku::getSkuId).collect(Collectors.toList());
        productFeignClient.updateSkuType(skuIdList, 1);
    }


    @Override
    public void saveSeckillSkuListToCache(String date) {

        MPJLambdaWrapper<SeckillSku> mpjLambdaWrapper = new MPJLambdaWrapper<>();
        mpjLambdaWrapper
                .selectAs(SeckillSku::getId,"seckillSkuId")
                .select(SeckillSku::getSkuId)
                .select(SeckillSku::getSkuName)
                .select(SeckillSku::getImgUrl)
                .select(SeckillSku::getSeckillPrice)
                .select(SeckillSku::getSeckillStock)
                .select(SeckillSku::getSeckillLimit)
                .select(SeckillSku::getSeckillSale)
                .selectAs(SeckillTime::getName,"timeName")
                .select(SeckillTime::getStartTime)
                .select(SeckillTime::getEndTime)
                .select(SeckillTime::getTimeStaus)
                .innerJoin(SeckillTime.class,SeckillTime::getId,SeckillSku::getSeckillTimeId)
                .gt(SeckillSku::getSeckillStock,0);
        List<SeckillSkuVo> seckillSkuVoList = seckillSkuMapper.selectJoinList(SeckillSkuVo.class,mpjLambdaWrapper);
        if(CollectionUtils.isEmpty(seckillSkuVoList)) {
            return;
        }

        for(SeckillSkuVo seckillSkuVo: seckillSkuVoList){
            //将秒杀sku信息放入缓存
            redisTemplate.boundHashOps(RedisConst.SECKILL_SKU_MAP).put(seckillSkuVo.getSkuId().toString(), seckillSkuVo);
            //将库存数保存到redis队列，防止超卖
            Integer seckillStock = seckillSkuVo.getSeckillStock();
            Long[] skuIdArray = new Long[seckillStock];
            for (Integer i = 0; i < seckillStock; i++) {
                skuIdArray[i] = seckillSkuVo.getSkuId();
            }
            redisTemplate.boundListOps(RedisConst.SECKILL_SKU_LIST + seckillSkuVo.getSkuId()).leftPushAll(skuIdArray);
            //设置过期时间
            //redisTemplate.expire(RedisConst.SECKILL_SKU_MAP, DateUtil.getCurrentExpireTimes(), TimeUnit.SECONDS);
        }
    }

    @Override
    public List<SeckillSkuVo> findSeckillSkuListFromCache(String timeName) {
        List<SeckillSkuVo> seckillSkuVoList = redisTemplate.boundHashOps(RedisConst.SECKILL_SKU_MAP).values();
        if(seckillSkuVoList.size() >1) {
            seckillSkuVoList.add(seckillSkuVoList.get(0));
            seckillSkuVoList.add(seckillSkuVoList.get(0));
            seckillSkuVoList.add(seckillSkuVoList.get(0));
            seckillSkuVoList.add(seckillSkuVoList.get(0));
        }
        return seckillSkuVoList;
        //return  seckillSkuVoList.stream().filter(seckillSkuVo -> timeName.equals( seckillSkuVo.getTimeName())).collect( Collectors.toList() );
    }

    @Override
    public SeckillSkuVo getSeckillSkuVo(Long skuId) {
        SeckillSkuVo seckillSkuVo = (SeckillSkuVo)redisTemplate.boundHashOps(RedisConst.SECKILL_SKU_MAP).get(skuId.toString());
        if(null == seckillSkuVo) {
            String date = new DateTime().toString("yyyy-MM-dd");
            this.saveSeckillSkuListToCache(date);
        }

        //场次状态 1：已开抢 2：抢购中 3：即将开抢
        Date currentDate = DateUtil.parseTime(new DateTime().toString("HH:mm:ss"));
        if(DateUtil.dateCompare(seckillSkuVo.getStartTime(), currentDate)) {
            seckillSkuVo.setTimeStaus(1);
        } else {
            //即将开抢
            seckillSkuVo.setTimeStaus(3);
        }
        return seckillSkuVo;
    }

    @Transactional
    @Override
    public Boolean checkAndMinusStock(List<SkuStockLockVo> skuStockLockVoList, String orderToken) {
        if (CollectionUtils.isEmpty(skuStockLockVoList)){
            throw new CustomExceptionHandler(ResultCodeEnum.DATA_ERROR);
        }

        // 遍历所有商品，验库存并锁库存，要具备原子性
        skuStockLockVoList.forEach(skuStockLockVo -> {
            checkLock(skuStockLockVo);
        });

        // 只要有一个商品锁定失败，所有锁定成功的商品要解锁库存
        if (skuStockLockVoList.stream().anyMatch(skuStockLockVo -> !skuStockLockVo.getIsLock())) {
            // 获取所有锁定成功的商品，遍历解锁库存
            skuStockLockVoList.stream().filter(SkuStockLockVo::getIsLock).forEach(skuStockLockVo -> {
//                seckillSkuMapper.rollBackStock(skuStockLockVo.getSkuId(), skuStockLockVo.getSkuNum());
                LambdaUpdateWrapper<SeckillSku> updateWrapper = new LambdaUpdateWrapper<>();
                updateWrapper
//                        .set(SeckillSku::getSeckillStock,skuStockLockVo.getSkuNum())
                        .apply(" seckill_lock_stock = seckill_lock_stock - "+ skuStockLockVo.getSkuNum())
                        .eq(SeckillSku::getSkuId,skuStockLockVo.getSkuId());
                this.update(updateWrapper);
            });
            // 响应锁定状态
            return false;
        }

        // 如果所有商品都锁定成功的情况下，需要缓存锁定信息到redis。以方便将来解锁库存 或者 减库存
        // 以orderToken作为key，以lockVos锁定信息作为value
        this.redisTemplate.opsForValue().set(RedisConst.STOCK_INFO + orderToken, skuStockLockVoList);

        // 锁定库存成功之后，定时解锁库存。
       amqpTemplate.convertAndSend("ORDER_EXCHANGE", "stock.ttl", orderToken);
        return true;
    }


    private void checkLock(SkuStockLockVo skuStockLockVo){
        RLock rLock = redissonClient
                .getFairLock(RedisConst.SECKILLSKUKEY_PREFIX + skuStockLockVo.getSkuId());
        rLock.lock();
        try{
            SeckillSkuVo seckillSkuVo = this.getSeckillSkuVo(skuStockLockVo.getSkuId());
            if(null == seckillSkuVo) {
                return;
            }

            LambdaQueryWrapper<SeckillSku> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(SeckillSku::getSkuId,skuStockLockVo.getSkuId());
            queryWrapper.apply("seckill_stock - seckill_lock_stock >=" +skuStockLockVo.getSkuNum());
            queryWrapper.last("for update");
            boolean exist =  seckillSkuMapper.selectOneDeep(queryWrapper) != null;


            if(!exist) {
                skuStockLockVo.setIsLock(false);
                return;
            }
            //锁库存
            LambdaUpdateWrapper<SeckillSku> lockQueryWrapper = new LambdaUpdateWrapper<>();
            lockQueryWrapper.eq(SeckillSku::getSkuId,skuStockLockVo.getSkuId());
            lockQueryWrapper.setSql("seckill_lock_stock = seckill_lock_stock +"+skuStockLockVo.getSkuNum());
            boolean isUpdate = this.update(lockQueryWrapper);
            if (isUpdate == true) {
                skuStockLockVo.setIsLock(true);
            }
        }finally {
            rLock.unlock();
        }

    }

    @Transactional
    @Override
    public void rollBackStock(String orderNo) {
        // 获取锁定库存的缓存信息
        List<SkuStockLockVo> skuStockLockVoList = (List<SkuStockLockVo>)this.redisTemplate.opsForValue().get(RedisConst.STOCK_INFO + orderNo);
        if (CollectionUtils.isEmpty(skuStockLockVoList)){
            return ;
        }

        // 回滚库存
        skuStockLockVoList.forEach(skuStockLockVo -> {
            SeckillSkuVo seckillSkuVo = this.getSeckillSkuVo(skuStockLockVo.getSkuId());
//            seckillSkuMapper.rollBackStock(seckillSkuVo.getSeckillSkuId(), skuStockLockVo.getSkuNum());
            LambdaUpdateWrapper<SeckillSku> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.set(SeckillSku::getSeckillStock,skuStockLockVo.getSkuNum())
                    .eq(SeckillSku::getId,seckillSkuVo.getSeckillSkuId());
            this.update(updateWrapper);



        });

        // 回滚库存之后，删除锁定库存的缓存。以防止重复解锁库存
        this.redisTemplate.delete(RedisConst.STOCK_INFO + orderNo);
    }

    @Override
    public boolean removeById(Serializable id) {
        SeckillSku seckillSku = this.getById(id);
        seckillSkuMapper.deleteById(id);

        //更新sku为普通商品
        List<Long> skuIdList = new ArrayList<>();
        skuIdList.add(seckillSku.getSkuId());
        productFeignClient.updateSkuType(skuIdList, SkuType.COMMON.getCode());
        return false;
    }
}
