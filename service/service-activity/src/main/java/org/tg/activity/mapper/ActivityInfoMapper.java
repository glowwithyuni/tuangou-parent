package org.tg.activity.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.ActivityInfo;
import org.tg.model.activity.ActivityRule;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-07 09:49:15
 * @Description
 * @Version 1.0
 */
@Repository
public interface ActivityInfoMapper extends MPJBaseMapper<ActivityInfo> {




}
