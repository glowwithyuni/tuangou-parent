package org.tg.activity.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.tg.activity.service.SeckillTimeService;
import org.tg.common.result.Result;
import org.tg.model.activity.SeckillTime;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-08 14:06:35
 * @Description 限时活动场次管理
 * @Version 1.0
 */
@Api(value = "限时活动场次管理", tags = "限时活动场次管理")
@RestController
@RequestMapping(value="/admin/activity/seckillTime")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SeckillTimeController {

    @Resource
    private SeckillTimeService seckillTimeService;

    @ApiOperation(value = "获取分页列表")
    @GetMapping("{seckillId}")
    public Result index(@PathVariable("seckillId") Integer seckillId) {

        LambdaQueryWrapper<SeckillTime> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SeckillTime::getSeckillId,seckillId);
        return Result.ok(seckillTimeService.list(queryWrapper));
    }

    @ApiOperation(value = "获取")
    @GetMapping("get/{id}")
    public Result get(@PathVariable Long id) {
        SeckillTime seckillSession = seckillTimeService.getById(id);
        return Result.ok(seckillSession);
    }

    @ApiOperation(value = "新增")
    @PostMapping("save")
    public Result save(@RequestBody SeckillTime seckillSession) {
        seckillTimeService.save(seckillSession);
        return Result.ok();
    }

    @ApiOperation(value = "修改")
    @PutMapping("update")
    public Result updateById(@RequestBody SeckillTime seckillSession) {
        seckillTimeService.updateById(seckillSession);
        return Result.ok();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        seckillTimeService.removeById(id);
        return Result.ok();
    }

    @ApiOperation(value = "根据id列表删除")
    @DeleteMapping("batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList) {
        seckillTimeService.removeByIds(idList);
        return Result.ok();
    }

    @ApiOperation(value = "更新状态")
    @PostMapping("updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        seckillTimeService.updateStatus(id, status);
        return Result.ok();
    }
}