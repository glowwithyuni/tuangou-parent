package org.tg.activity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.ActivityRule;

/**
 * @Author Glow
 * @Date 2023-07-07 09:49:37
 * @Description
 * @Version 1.0
 */
@Repository
public interface ActivityRuleMapper  extends BaseMapper<ActivityRule> {
}
