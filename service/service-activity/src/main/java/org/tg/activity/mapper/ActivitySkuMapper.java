package org.tg.activity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.ActivitySku;

/**
 * @Author Glow
 * @Date 2023-07-07 09:49:48
 * @Description
 * @Version 1.0
 */
@Repository
public interface ActivitySkuMapper extends MPJBaseMapper<ActivitySku> {
}
