package org.tg.activity.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.SeckillSku;

/**
 * @Author Glow
 * @Date 2023-07-08 14:16:01
 * @Description
 * @Version 1.0
 */
@Repository
public interface SeckillSkuMapper extends MPJBaseMapper<SeckillSku> {
}
