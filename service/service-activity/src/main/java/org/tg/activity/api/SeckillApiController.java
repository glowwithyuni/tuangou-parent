package org.tg.activity.api;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tg.activity.service.SeckillSkuService;
import org.tg.activity.service.SeckillTimeService;
import org.tg.common.result.Result;
import org.tg.model.activity.SeckillSku;
import org.tg.vo.activity.SeckillSkuVo;
import org.tg.vo.product.SkuStockLockVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-15 21:04:25
 * @Description
 * @Version 1.0
 */
@RestController
@RequestMapping("/api/seckill")
public class SeckillApiController {
    @Autowired
    private SeckillTimeService seckillTimeService;
    @Autowired
    private SeckillSkuService seckillSkuService;
    @GetMapping("/checkAndMinusStock/{orderNo}")
    Boolean checkAndMinusStock( @RequestBody  List<SkuStockLockVo> seckillStockLockVoList,
                                @PathVariable("orderNo") String orderNo)
    {
        return seckillSkuService.checkAndMinusStock(seckillStockLockVoList,orderNo);
    }
    @ApiOperation(value = "获取用户端首页秒杀数据")
    @GetMapping("/getSeckillSkuVo/{id}")
    private SeckillSkuVo getSeckillSkuVo(@PathVariable("id") long id)
    {
        return seckillSkuService.getSeckillSkuVo(id);
    }
    @ApiOperation(value = "获取用户端首页秒杀数据")
    @GetMapping("/findHomeData")
    public Map<String, Object> findHomeData() {
        return    seckillTimeService.findHomeData();

    }
    @ApiOperation(value = "根据商品ID获取秒杀商品数据")
    @GetMapping("/getSeckillSkuBySkuId/{skuId}")
    private SeckillSku  getSeckillSkuBySkuId(@PathVariable("skuId") Integer skuId)
    {
        LambdaQueryWrapper<SeckillSku> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SeckillSku::getSkuId,skuId);
       return seckillSkuService.getOne(queryWrapper);
    }
}
