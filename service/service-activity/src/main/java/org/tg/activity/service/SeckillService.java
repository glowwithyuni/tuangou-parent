package org.tg.activity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.activity.Seckill;
import org.tg.vo.activity.SeckillQueryVo;

/**
 * @Author Glow
 * @Date 2023-07-08 14:12:19
 * @Description
 * @Version 1.0
 */
public interface SeckillService extends IService<Seckill> {

    IPage<Seckill> selectPage(Page<Seckill> pageParam, SeckillQueryVo seckillQueryVo);

    void updateStatus(Long id, Integer status);
}