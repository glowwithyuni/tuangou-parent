package org.tg.activity.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.activity.ActivityInfo;
import org.tg.model.activity.ActivityRule;
import org.tg.model.order.CartInfo;
import org.tg.model.product.SkuInfo;
import org.tg.vo.activity.ActivityRuleVo;
import org.tg.vo.order.CartInfoVo;
import org.tg.vo.order.OrderConfirmVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-07 09:53:01
 * @Description 优惠活动信息服务
 * @Version 1.0
 */
public interface ActivityInfoService extends IService<ActivityInfo> {

    OrderConfirmVo findCartActivityAndCoupon(List<CartInfo> cartInfoList,
                                             Long userId);
    List<CartInfoVo> findCartActivityList(List<CartInfo> cartInfoList);
    //列表
    IPage<ActivityInfo> selectPage(Page<ActivityInfo> pageParam);

    //1 根据活动id获取活动规则数据
    Map<String, Object> findActivityRuleList(Long activityId);

    //2 在活动里面添加规则数据
    void saveActivityRule(ActivityRuleVo activityRuleVo);

    //3 根据关键字查询匹配sku信息
    List<SkuInfo> findSkuInfoByKeyword(String keyword);

    //根据skuId列表获取促销信息
    Map<Long, List<String>> findActivity(List<Long> skuIdList);

    //根据skuID获取营销数据和优惠卷
    Map<String, Object> findActivityAndCoupon(Long skuId, Long userId);

    //根据skuId获取活动规则数据
    List<ActivityRule> findActivityRuleBySkuId(Long skuId);


}
