package org.tg.activity.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.SeckillTime;

/**
 * @Author Glow
 * @Date 2023-07-08 14:25:51
 * @Description
 * @Version 1.0
 */
@Repository
public interface SeckillTimeMapper extends MPJBaseMapper<SeckillTime> {
}
