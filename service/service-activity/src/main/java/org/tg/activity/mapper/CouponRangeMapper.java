package org.tg.activity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.activity.CouponRange;

/**
 * @Author Glow
 * @Date 2023-07-07 09:50:17
 * @Description
 * @Version 1.0
 */
@Repository
public interface CouponRangeMapper extends BaseMapper<CouponRange> {
}
