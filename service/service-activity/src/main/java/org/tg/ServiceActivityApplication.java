package org.tg;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author Glow
 * @Date 2023-07-06 23:55:46
 * @Description 营销活动服务启动
 * @Version 1.0
 */

@EnableDiscoveryClient
@EnableFeignClients(basePackages = "org.tg.client.*")
@SpringBootApplication
@MapperScan(basePackages = "org.tg.activity.mapper")
public class ServiceActivityApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceActivityApplication.class, args);
    }
}
