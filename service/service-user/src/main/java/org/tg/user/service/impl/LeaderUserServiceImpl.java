package org.tg.user.service.impl;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tg.client.sys.RegionFeignClient;
import org.tg.model.user.Leader;
import org.tg.model.user.LeaderUser;
import org.tg.model.user.UserDelivery;
import org.tg.user.mapper.LeaderUserMapper;
import org.tg.user.service.LeaderService;
import org.tg.user.service.LeaderUserService;
import org.tg.user.service.UserDeliveryService;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-07-18 12:04:48
 * @Description
 * @Version 1.0
 */
@Service
public class LeaderUserServiceImpl extends ServiceImpl<LeaderUserMapper, LeaderUser> implements LeaderUserService {
    @Autowired
    private LeaderUserMapper leaderUserMapper;
    @Autowired
    private UserDeliveryService userDeliveryService;
    //还要保存仓库信息
    @Autowired
    private RegionFeignClient regionFeignClient;
    @Autowired
    private LeaderService leaderService;
    @Override
    public void saveLeaderUser(Long leaderId, Long userId) {
        //选择（添加）提货点信息前需要先删除前边选择的提货点
        LambdaQueryWrapper<LeaderUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(LeaderUser::getUserId,userId);
       // queryWrapper.eq(LeaderUser::getLeaderId,leaderId);
        leaderUserMapper.delete(queryWrapper);
//        LambdaUpdateWrapper<LeaderUser> updateWrapper = new LambdaUpdateWrapper<>();
//        updateWrapper.eq(LeaderUser::getUserId,userId);
//        updateWrapper.eq(LeaderUser::getLeaderId,leaderId);
//        updateWrapper.set(LeaderUser::getIsDeleted,1);
//        this.update(updateWrapper);
        //同时还要删除userDelievery的仓库团长用户信息
        LambdaQueryWrapper<UserDelivery> userDeliveryqueryWrapper = new LambdaQueryWrapper<>();
        userDeliveryqueryWrapper.eq(UserDelivery::getUserId,userId);
     //   userDeliveryqueryWrapper.eq(UserDelivery::getLeaderId,leaderId);
            userDeliveryService.remove(userDeliveryqueryWrapper);
        //       LeaderUser exist = new LeaderUser();
//       exist = this.getOne(queryWrapper);
//                if(exist == null)
//                {
                    LeaderUser leaderUser = new LeaderUser();
                    leaderUser.setLeaderId(leaderId);
                    leaderUser.setUserId(userId);
                    this.save(leaderUser);
                    //存储后还要保存用户团长对应的仓库信息wareId
                    UserDelivery userDelivery = new UserDelivery();
        userDelivery.setLeaderId(leaderId);
        userDelivery.setUserId(userId);
        //获取Leader的区域ID来获取仓库ID
        Leader leader = leaderService.getById(leaderId);

        userDelivery.setWareId(regionFeignClient.getWareId(leader.getRegionId()));
//                }
//            return  leaderUser;
        userDeliveryService.save(userDelivery);
    }
    //因为没有用服务器搭建，测试环境下因为性能太差导致事务前脚刚修改，后脚读取数据抛出异常容易幻读
    //可在小程序端延时操作也可避免冲突
    @Override
    public Long getLeaderIdByUserId(Long userId) {

        LambdaQueryWrapper<LeaderUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.select(LeaderUser::getLeaderId);
        queryWrapper.eq(LeaderUser::getUserId,userId);

        return leaderUserMapper.selectOne(queryWrapper).getLeaderId();
    }
}
