package org.tg.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.user.UserDelivery;

/**
 * @Author Glow
 * @Date 2023-07-11 14:31:26
 * @Description
 * @Version 1.0
 */
@Repository
public interface UserDeliveryMapper extends BaseMapper<UserDelivery> {

}
