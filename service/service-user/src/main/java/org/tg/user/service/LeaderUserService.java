package org.tg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.user.LeaderUser;

/**
 * @Author Glow
 * @Date 2023-07-18 12:04:25
 * @Description
 * @Version 1.0
 */
public interface LeaderUserService extends IService<LeaderUser> {
    void saveLeaderUser(Long leaderId,Long userId);
    //根据userId获取指定leaderId
    Long getLeaderIdByUserId(Long userId);
}
