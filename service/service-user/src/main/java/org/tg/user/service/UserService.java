package org.tg.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.user.User;
import org.tg.vo.user.LeaderAddressVo;
import org.tg.vo.user.UserLoginVo;

/**
 * @Author Glow
 * @Date 2023-07-11 14:25:47
 * @Description
 * @Version 1.0
 */
public interface UserService extends IService<User> {
    LeaderAddressVo getLeaderAddressVoByUserId(Long userId);

    /**
     * 根据微信openid获取用户信息
     * @param openId
     * @return
     */
    User getByOpenid(String openId);

    /**
     * 获取当前登录用户信息
     * @param userId
     * @return
     */
    UserLoginVo getUserLoginVo(Long userId);
}
