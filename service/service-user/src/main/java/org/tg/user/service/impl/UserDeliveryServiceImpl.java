package org.tg.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.user.UserDelivery;
import org.tg.user.mapper.UserDeliveryMapper;
import org.tg.user.service.UserDeliveryService;

/**
 * @Author Glow
 * @Date 2023-07-21 20:07:37
 * @Description
 * @Version 1.0
 */
@Service
public class UserDeliveryServiceImpl extends ServiceImpl<UserDeliveryMapper, UserDelivery> implements UserDeliveryService {
    @Override
    public UserDelivery getUserDeliveryByUserIdAndLeaderId(Long userId, Long leaderId) {
        LambdaQueryWrapper<UserDelivery> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserDelivery::getUserId,userId);
        queryWrapper.eq(UserDelivery::getLeaderId,leaderId);
        return this.getOne(queryWrapper);
    }
}
