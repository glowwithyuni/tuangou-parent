package org.tg.user.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.user.LeaderUser;

/**
 * @Author Glow
 * @Date 2023-07-18 12:01:03
 * @Description
 * @Version 1.0
 */
@Repository
public interface LeaderUserMapper extends MPJBaseMapper<LeaderUser> {

}
