package org.tg.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.tg.common.result.Result;
import org.tg.model.user.Leader;
import org.tg.user.mapper.LeaderMapper;
import org.tg.user.service.LeaderService;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-16 14:38:08
 * @Description 团长管理接口
 * @Version 1.0
 */
@RestController
@Api("团长管理接口")
@RequestMapping("/admin/user/leader")
public class LeadController {
    @Autowired
    private LeaderService leaderService;
    @ApiOperation("新建团长审核信息")
    @PostMapping("/save")
    private Result save(@RequestBody Leader leader)
    {
        leaderService.save(leader);
       return Result.ok();
    }
    @ApiOperation("根据ID返回团长信息")
    @GetMapping("/get/{id}")
    public Result get(@ApiParam(name = "id", value = "当前ID", required = true)
                          @PathVariable Integer id)
    {
        return Result.ok(leaderService.getById(id));
    }
    @ApiOperation("修改团长信息")
    @PutMapping("/update")
    public Result update(@RequestBody Leader leader)
    {
        return Result.ok(leaderService.updateById(leader));
    }
    @ApiOperation("审核团长是否通过1过0不通过")
    @PostMapping("/check/{id}/{status}")
    public Result check(
            @ApiParam(name = "id", value = "当前ID", required = true)
            @PathVariable Integer id,
            @ApiParam(name = "status", value = "审核状态", required = true)
            @PathVariable Integer status
    ){
        leaderService.check(id,status);
        return Result.ok();
    }
    @ApiOperation("加载审核团长列表")
    @GetMapping("/checkList/{page}/{limit}")
    public Result checkList(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit
    )
    {
        Page<Leader> pages = new Page<>(page,limit);
        IPage<Leader> iPage = leaderService.checkList(pages,0);
        return Result.ok(iPage);
    }
    @ApiOperation("分页加载审核团长列表")
    @GetMapping("/list/{page}/{limit}")
    public Result pagelist(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit
    )
    {
        Page<Leader> pages = new Page<>(page,limit);
        IPage<Leader> iPage = leaderService.List(pages,1);
        return Result.ok(iPage);
    }
    @ApiOperation("加载审核团长列表")
    @GetMapping("/list")
    public List<Leader> list()
    {
       LambdaQueryWrapper<Leader> queryWrapper = new LambdaQueryWrapper<>();
       queryWrapper.eq(Leader::getCheckStatus,1);
        return leaderService.list(queryWrapper);
    }
    @ApiOperation("加载已审核团长列表")
    @GetMapping("/getLeaderByName/{keyword}")
    public Result list(
            @ApiParam(name = "keyword", value = "关键字", required = true)
            @PathVariable String keyword
    )
    {
        LambdaQueryWrapper<Leader> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Leader::getName,keyword)
        .eq(Leader::getCheckStatus,1);

        return Result.ok(leaderService.list(queryWrapper));
    }
    @ApiOperation("加载未审核团长列表")
    @GetMapping("/getCheckLeaderByName/{keyword}")
    public Result checklist(
            @ApiParam(name = "keyword", value = "关键字", required = true)
            @PathVariable String keyword
    )
    {
        LambdaQueryWrapper<Leader> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Leader::getName,keyword)
                .eq(Leader::getCheckStatus,0);

        return Result.ok(leaderService.list(queryWrapper));
    }
}
