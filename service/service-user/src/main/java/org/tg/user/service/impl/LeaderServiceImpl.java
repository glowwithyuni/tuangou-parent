package org.tg.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.tg.client.sys.RegionFeignClient;
import org.tg.model.user.Leader;
import org.tg.user.mapper.LeaderMapper;
import org.tg.user.service.LeaderService;

import java.io.DataInput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-16 14:52:01
 * @Description
 * @Version 1.0
 */
@Service
public class LeaderServiceImpl extends ServiceImpl<LeaderMapper, Leader> implements LeaderService {
    @Autowired
    private RegionFeignClient regionFeignClient;
    @Override
    public IPage<Leader> List(Page<Leader> pages, Integer status) {
        LambdaQueryWrapper<Leader> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Leader::getCheckStatus,status);
        IPage<Leader> leaderIPage = this.page(pages,queryWrapper);
        List<Leader> lists = leaderIPage.getRecords();
         for(int i = 0; i < lists.size(); i++)
         {
             Long province = lists.get(i).getProvince();
             Long city = lists.get(i).getCity();
             Long district = lists.get(i).getDistrict();
             String provinceName = regionFeignClient.getRegionById(province).getName();
             String cityName = regionFeignClient.getRegionById(city).getName();
             String districtName = regionFeignClient.getRegionById(district).getName();
             Map<String,Object> map = new HashMap<>();
             map.put("regionName",provinceName+cityName+districtName);
             lists.get(i).setParam(map);
         }
         leaderIPage.setRecords(lists);
         return leaderIPage;
    }

    @Override
    public IPage<Leader> checkList(Page<Leader> pages, Integer status) {
        LambdaQueryWrapper<Leader> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Leader::getCheckStatus,status);
        IPage<Leader> leaderIPage = this.page(pages,queryWrapper);
        List<Leader> lists = leaderIPage.getRecords();
        for(int i = 0; i < lists.size(); i++)
        {
            Long province = lists.get(i).getProvince();
            Long city = lists.get(i).getCity();
            Long district = lists.get(i).getDistrict();
            String provinceName = regionFeignClient.getRegionById(province).getName();
            String cityName = regionFeignClient.getRegionById(city).getName();
            String districtName = regionFeignClient.getRegionById(district).getName();
            Map<String,Object> map = new HashMap<>();
            map.put("provinceName",provinceName);
            map.put("cityName",cityName);
            map.put("districtName",districtName);
            lists.get(i).setParam(map);
        }
        leaderIPage.setRecords(lists);
        return leaderIPage;
    }

    @Override
    public void check(Integer id, Integer checkStatus) {
        LambdaUpdateWrapper<Leader> queryWrapper = new LambdaUpdateWrapper<>();
        queryWrapper.eq(Leader::getId,id);
        queryWrapper.set(Leader::getCheckStatus,checkStatus);
        this.update(queryWrapper);
    }
}
