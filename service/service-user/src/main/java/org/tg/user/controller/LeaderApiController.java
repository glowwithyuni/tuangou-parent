package org.tg.user.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.common.auth.AuthContextHolder;
import org.tg.common.result.Result;
import org.tg.model.user.Leader;
import org.tg.model.user.LeaderUser;
import org.tg.model.user.UserDelivery;
import org.tg.user.service.LeaderService;
import org.tg.user.service.LeaderUserService;
import org.tg.user.service.UserDeliveryService;
import org.tg.user.service.UserService;
import org.tg.vo.user.LeaderAddressVo;

import javax.annotation.Resource;

/**
 * @Author Glow
 * @Date 2023-07-14 16:10:02
 * @Description
 * @Version 1.0
 */
@Api(tags = "团长接口")
@RestController
@RequestMapping("/api/user/leader")
public class LeaderApiController {
    @Autowired
    private UserDeliveryService userDeliveryService;
    @Autowired
    private LeaderUserService leaderUserService;
    @Autowired
    private LeaderService leaderService;
    @Autowired
    private UserService userService;
    @ApiOperation("根据Id获取提货仓库信息")
    @GetMapping("/getUserDeliveryByUserIdAndLeaderId/{leaderId}/{userId}")
    public UserDelivery getUserDeliveryByUserIdAndLeaderId(
            @PathVariable("userId") Long userId,
            @PathVariable("leaderId") Long leaderId)
    {
        return userDeliveryService.getUserDeliveryByUserIdAndLeaderId(userId,leaderId);
    }
    @ApiOperation("根据Id获取团长信息")
    @GetMapping("/getLeaderById/{leaderId}")
    public Leader getLeaderById(@PathVariable("leaderId") Long leaderId)
    {
       return leaderService.getById(leaderId);
    }
    @ApiOperation("选定提货点")
    @GetMapping("/auth/selectLeader/{leaderId}")
    public Result selectLeader(
            @PathVariable("leaderId")Long leaderId
    )
    {
      leaderUserService.saveLeaderUser(leaderId,AuthContextHolder.getUserId());
        return Result.ok();
    }

    @ApiOperation("提货点地址信息")
    @GetMapping("/inner/getUserAddressByUserId/{userId}")
    public LeaderAddressVo getLeaderAddressVoByUserId(@PathVariable(value = "userId") Long userId) {
        return userService.getLeaderAddressVoByUserId(userId);
    }
    @ApiOperation("获取用户选定提货点团长ID")
    @GetMapping("/inner/getLeaderIdByUserId/{userId}")
    public Long getLeaderIdByUserId(@PathVariable(value = "userId") Long userId)
    {
        return leaderUserService.getLeaderIdByUserId(userId);
    }
}
