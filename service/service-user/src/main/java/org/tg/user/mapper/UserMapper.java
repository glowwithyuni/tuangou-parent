package org.tg.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.user.User;

/**
 * @Author Glow
 * @Date 2023-07-11 14:32:14
 * @Description
 * @Version 1.0
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
