package org.tg.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.user.Leader;

/**
 * @Author Glow
 * @Date 2023-07-16 14:51:37
 * @Description
 * @Version 1.0
 */
public interface LeaderService extends IService<Leader> {
    IPage<Leader> List(Page<Leader> pages,Integer status);
    IPage<Leader> checkList(Page<Leader> pages,Integer status);
      void check(Integer id,Integer checkStatus);
}
