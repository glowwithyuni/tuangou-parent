package org.tg.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.user.UserDelivery;

/**
 * @Author Glow
 * @Date 2023-07-21 20:06:20
 * @Description
 * @Version 1.0
 */
public interface UserDeliveryService extends IService<UserDelivery> {
    public UserDelivery getUserDeliveryByUserIdAndLeaderId(Long userId,Long leaderId);
}
