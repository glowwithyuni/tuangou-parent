package org.tg;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author Glow
 * @Date 2023-07-30 10:19:09
 * @Description
 * @Version 1.0
 */
@EnableFeignClients
@SpringBootApplication
public class ServicePaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServicePaymentApplication.class, args);
        
    }
}
