package org.tg.payment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.tg.model.order.PaymentInfo;

/**
 * @Author Glow
 * @Date 2023-08-01 10:24:25
 * @Description
 * @Version 1.0
 */
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {

}
