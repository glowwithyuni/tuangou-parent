package org.tg.payment.service;

import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-08-01 10:15:25
 * @Description
 * @Version 1.0
 */
public interface WeixinService {

    /**
     * 根据订单号下单，生成支付链接
     * @param orderNo
     * @return
     */
    Map createJsapi(String orderNo);

    /**
     * 根据订单号去微信第三方查询支付状态
     * @param orderNo
     * @return
     */
    Map queryPayStatus(String orderNo, String paymentType);

}
