package org.tg.payment.mapper;

import org.tg.enums.PaymentType;
import org.tg.model.order.PaymentInfo;

import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-08-01 10:30:13
 * @Description
 * @Version 1.0
 */
public interface PaymentService {

    /**
     * 保存交易记录
     * @param orderNo
     * @param paymentType 支付类型（1：微信 2：支付宝）
     */
    PaymentInfo savePaymentInfo(String orderNo, PaymentType paymentType);

    PaymentInfo getPaymentInfo(String orderNo, PaymentType paymentType);

    //支付成功
    void paySuccess(String orderNo,PaymentType paymentType, Map<String,String> paramMap);
}
