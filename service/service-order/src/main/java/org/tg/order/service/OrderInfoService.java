package org.tg.order.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.order.OrderInfo;
import org.tg.vo.order.OrderConfirmVo;
import org.tg.vo.order.OrderSubmitVo;

/**
 * @Author Glow
 * @Date 2023-07-25 14:46:30
 * @Description
 * @Version 1.0
 */
public interface OrderInfoService extends IService<OrderInfo> {
    /**
     * 确认订单
     */
    OrderConfirmVo confirmOrder();
    //生成订单
    Long submitOrder(OrderSubmitVo orderSubmitVo);

    //订单详情
    OrderInfo getOrderInfoById(Long orderId);
    //根据订单状态获取分页订单详情
    Page<OrderInfo> findUserOrderPage(Long userId, Integer pageNum, Integer limit, Integer status);
    void orderPay(String orderNo);
    OrderInfo getOrderInfoByOrderNo(String orderNo);
}
