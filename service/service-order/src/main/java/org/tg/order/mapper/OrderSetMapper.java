package org.tg.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.order.OrderSet;

/**
 * @Author Glow
 * @Date 2023-07-25 15:02:39
 * @Description
 * @Version 1.0
 */
@Repository
public interface OrderSetMapper extends BaseMapper<OrderSet> {
}
