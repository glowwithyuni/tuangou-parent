package org.tg.order.controller.api;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tg.common.auth.AuthContextHolder;
import org.tg.common.result.Result;
import org.tg.enums.OrderStatus;
import org.tg.model.order.OrderInfo;
import org.tg.order.service.OrderInfoService;
import org.tg.vo.order.OrderSubmitVo;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author Glow
 * @Date 2023-07-25 14:43:53
 * @Description
 * @Version 1.0
 */
@Api(value = "Order管理", tags = "Order管理")
@RestController
@RequestMapping(value="/api/order")
public class OrderApiController {

    @Autowired
    private OrderInfoService orderService;

    @ApiOperation("获取订单分页列表")
    @GetMapping("auth/findUserOrderPage/{pageNum}/{limit}")
    public Result findUserOrderPage(
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("limit") Integer limit,
            @RequestParam("orderStatus") OrderStatus orderStatus
            )
    {
       Long userId = AuthContextHolder.getUserId();
      Integer status = orderStatus.getCode();
     return Result.ok( orderService.findUserOrderPage(userId,pageNum,limit,status));
    }
    @ApiOperation("确认订单")
    @GetMapping("auth/confirmOrder")
    public Result confirm() {
        return Result.ok(orderService.confirmOrder());
    }
    @ApiOperation("生成订单")
    @PostMapping("auth/submitOrder")
    public Result submitOrder(@RequestBody OrderSubmitVo orderParamVo, HttpServletRequest request) {
        // 获取到用户Id
        Long userId = AuthContextHolder.getUserId();
        return Result.ok(orderService.submitOrder(orderParamVo));
    }
    @ApiOperation("获取订单详情")
    @GetMapping("auth/getOrderInfoById/{orderId}")
    public Result getOrderInfoById(@PathVariable("orderId") Long orderId){
        return Result.ok(orderService.getOrderInfoById(orderId));
    }
    //根据orderNo查询订单信息
    @GetMapping("inner/getOrderInfo/{orderNo}")
    public OrderInfo getOrderInfo(@PathVariable("orderNo") String orderNo) {
        OrderInfo orderInfo = orderService.getOrderInfoByOrderNo(orderNo);
        return orderInfo;
    }
}
