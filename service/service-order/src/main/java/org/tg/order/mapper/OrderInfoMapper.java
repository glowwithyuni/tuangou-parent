package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.order.OrderInfo;

/**
 * @Author Glow
 * @Date 2023-07-25 14:48:54
 * @Description
 * @Version 1.0
 */
@Repository
public interface OrderInfoMapper extends MPJBaseMapper<OrderInfo> {
}
