package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.order.OrderLog;

/**
 * @Author Glow
 * @Date 2023-07-25 14:58:07
 * @Description
 * @Version 1.0
 */
public interface OrderLogService extends IService<OrderLog> {
}
