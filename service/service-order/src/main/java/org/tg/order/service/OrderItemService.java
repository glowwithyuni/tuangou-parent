package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.order.OrderItem;

/**
 * @Author Glow
 * @Date 2023-07-25 14:55:47
 * @Description
 * @Version 1.0
 */
public interface OrderItemService extends IService<OrderItem> {
}
