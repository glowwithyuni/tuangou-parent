package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.order.OrderItem;

/**
 * @Author Glow
 * @Date 2023-07-25 14:55:24
 * @Description
 * @Version 1.0
 */
@Repository
public interface OrderItemMapper extends MPJBaseMapper<OrderItem> {
}
