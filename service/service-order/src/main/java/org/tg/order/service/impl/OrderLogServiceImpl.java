package org.tg.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.order.OrderLog;
import org.tg.order.mapper.OrderLogMapper;
import org.tg.order.service.OrderLogService;

/**
 * @Author Glow
 * @Date 2023-07-25 15:01:56
 * @Description
 * @Version 1.0
 */
@Service
public class OrderLogServiceImpl extends ServiceImpl<OrderLogMapper, OrderLog> implements OrderLogService {

}
