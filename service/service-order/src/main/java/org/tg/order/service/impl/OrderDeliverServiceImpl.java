package org.tg.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.order.OrderDeliver;
import org.tg.order.mapper.OrderDeliverMapper;
import org.tg.order.service.OrderDeliverService;

/**
 * @Author Glow
 * @Date 2023-07-25 15:07:17
 * @Description
 * @Version 1.0
 */
@Service
public class OrderDeliverServiceImpl extends ServiceImpl<OrderDeliverMapper, OrderDeliver> implements OrderDeliverService {
}
