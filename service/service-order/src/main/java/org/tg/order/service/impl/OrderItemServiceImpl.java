package org.tg.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.order.OrderItem;
import org.tg.order.mapper.OrderItemMapper;
import org.tg.order.service.OrderItemService;

/**
 * @Author Glow
 * @Date 2023-07-25 14:56:11
 * @Description
 * @Version 1.0
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements OrderItemService {
}
