package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.order.OrderLog;

/**
 * @Author Glow
 * @Date 2023-07-25 14:57:41
 * @Description
 * @Version 1.0
 */
@Repository
public interface OrderLogMapper extends MPJBaseMapper<OrderLog> {
}
