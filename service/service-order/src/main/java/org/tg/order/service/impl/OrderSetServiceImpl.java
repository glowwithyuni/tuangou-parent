package org.tg.order.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.tg.model.order.OrderSet;
import org.tg.order.mapper.OrderSetMapper;
import org.tg.order.service.OrderSetService;

/**
 * @Author Glow
 * @Date 2023-07-25 15:05:19
 * @Description
 * @Version 1.0
 */
@Service
public class OrderSetServiceImpl extends ServiceImpl<OrderSetMapper, OrderSet> implements OrderSetService {
}
