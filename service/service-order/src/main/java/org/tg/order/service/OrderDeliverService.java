package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.order.OrderDeliver;

/**
 * @Author Glow
 * @Date 2023-07-25 15:07:08
 * @Description
 * @Version 1.0
 */
public interface OrderDeliverService  extends IService<OrderDeliver> {
}
