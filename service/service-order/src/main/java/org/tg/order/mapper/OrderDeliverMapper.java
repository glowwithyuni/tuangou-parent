package org.tg.order.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.springframework.stereotype.Repository;
import org.tg.model.order.OrderDeliver;

/**
 * @Author Glow
 * @Date 2023-07-25 15:06:40
 * @Description
 * @Version 1.0
 */
@Repository
public interface OrderDeliverMapper extends MPJBaseMapper<OrderDeliver> {
}
