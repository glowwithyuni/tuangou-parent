package org.tg.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.tg.model.order.OrderSet;

/**
 * @Author Glow
 * @Date 2023-07-25 15:05:02
 * @Description
 * @Version 1.0
 */
public interface OrderSetService extends IService<OrderSet> {
}
