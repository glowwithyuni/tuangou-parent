package org.tg.order.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tg.common.result.Result;
import org.tg.model.order.OrderInfo;
import org.tg.order.service.OrderInfoService;
import org.tg.vo.order.OrderQueryVo;

@RequestMapping("/admin/order/orderInfo")
@RestController
public class OrderInfoController {
    @Autowired
    private OrderInfoService orderInfoService;
    @GetMapping("/{page}/{limit}")
    public Result pagelist(
            @ApiParam(name = "page", value = "当前页码", required = true)
            @PathVariable Long page,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            OrderQueryVo  orderQueryVo
    )
    {
        System.out.println("所有的订单信息："+orderQueryVo.toString());
        Page<OrderInfo> pages = new Page<>(page,limit);
        LambdaQueryWrapper<OrderInfo> queryWrapper = new LambdaQueryWrapper<>();
        if(!StringUtils.isEmpty(orderQueryVo.getOutTradeNo()))
        {
            queryWrapper.like(OrderInfo::getOrderNo,orderQueryVo.getOutTradeNo());

        }
        if(orderQueryVo.getOrderStatus()!= null)
        {
            queryWrapper.eq(OrderInfo::getOrderStatus,orderQueryVo.getOrderStatus());
        }
        if(orderQueryVo.getLeaderId()!=null)
         queryWrapper.eq(OrderInfo::getLeaderId,orderQueryVo.getLeaderId());
        if(orderQueryVo.getWareId()!=null)
        queryWrapper.eq(OrderInfo::getWareId,orderQueryVo.getWareId());
        if(orderQueryVo.getCreateTimeBegin()!=null)
        queryWrapper.le(OrderInfo::getCreateTime,orderQueryVo.getCreateTimeBegin());
        if(orderQueryVo.getCreateTimeEnd()!=null)
        queryWrapper.ge(OrderInfo::getCreateTime,orderQueryVo.getCreateTimeEnd());
        if(!StringUtils.isEmpty(orderQueryVo.getReceiver()))
        {
            if(StringUtils.isNumeric(orderQueryVo.getReceiver()))
            {
                queryWrapper.like(OrderInfo::getReceiverPhone,orderQueryVo.getReceiver());
            }
            else {
                queryWrapper.like(OrderInfo::getReceiverName,orderQueryVo.getReceiver());
            }
        }
        return Result.ok(orderInfoService.page(pages,queryWrapper));
    }

    @GetMapping("/get/{id}")
    public Result getOrderInfo(@PathVariable("id") Long id)
    {
    return  Result.ok(orderInfoService.getOrderInfoById(id))    ;
    }
}
