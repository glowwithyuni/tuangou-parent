package org.tg.search.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.tg.model.search.SkuEs;
import org.tg.vo.search.SkuEsQueryVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-04 14:53:58
 * @Description
 * @Version 1.0
 */
public interface SkuService {
    /**
     * 搜索列表
     * @param skuEsQueryVo
     * @return
     */
    Page<SkuEs> search(Pageable pageable, SkuEsQueryVo skuEsQueryVo);
    /**
     * 获取热门商品
     * @return
     */
    List<SkuEs> findHotSkuList();
    /**
     * 上架商品列表
     * @param skuId
     */
    void upperSku(Long skuId);

    /**
     * 下架商品列表
     * @param skuId
     */
    void lowerSku(Long skuId);
    //更新商品热度
    void incrHotScore(Long skuId);
}
