package org.tg.search.repository;

import org.springframework.data.repository.CrudRepository;
import org.tg.model.search.LeaderEs;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-17 23:32:49
 * @Description
 * @Version 1.0
 */
public interface LeaderCrudRepository extends CrudRepository<LeaderEs,Long> {
    List<LeaderEs> findAll();
}
