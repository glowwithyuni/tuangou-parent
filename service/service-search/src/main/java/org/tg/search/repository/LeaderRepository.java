package org.tg.search.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.tg.model.search.LeaderEs;

/**
 * @Author Glow
 * @Date 2023-07-17 22:28:57
 * @Description
 * @Version 1.0
 */
public interface LeaderRepository  extends ElasticsearchRepository<LeaderEs,Long> {

}
