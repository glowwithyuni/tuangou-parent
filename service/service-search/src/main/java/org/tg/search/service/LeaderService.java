package org.tg.search.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.data.domain.Page;
import org.tg.model.search.LeaderEs;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-17 22:33:47
 * @Description
 * @Version 1.0
 */
public interface LeaderService {
    List<LeaderEs> saveAllLeaderEs();
    Page<LeaderEs> getBoundLeaders(Integer page, Integer limit, Double longitude, Double latitude);
}
