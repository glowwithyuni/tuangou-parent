package org.tg.search.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.tg.model.search.SkuEs;

/**
 * @Author Glow
 * @Date 2023-07-04 14:55:42
 * @Description 商品仓库搜索
 * @Version 1.0
 */
public interface SkuRepository extends ElasticsearchRepository<SkuEs, Long> {


    Page<SkuEs> findByCategoryIdAndWareId(Long categoryId, Long wareId, Pageable page);

    Page<SkuEs> findByKeywordAndWareId(String keyword, Long wareId, Pageable page);
    //获取爆品商品
    Page<SkuEs> findByOrderByHotScoreDesc(Pageable page);
}
