package org.tg.search.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tg.common.result.Result;
import org.tg.search.service.LeaderService;

/**
 * @Author Glow
 * @Date 2023-07-17 22:18:05
 * @Description
 * @Version 1.0
 */
@RestController
@RequestMapping("api/search/leader")
public class LeadApiController {
    @Autowired
    private LeaderService leaderService;
    @GetMapping("/{page}/{limit}")
     public Result list(
            @PathVariable("page")Integer page,
            @PathVariable("limit")Integer limit,
            @RequestParam(value = "longitude")Double longitude,
            @RequestParam(value = "latitude")Double latitude
            )
    {
         return Result.ok(leaderService.getBoundLeaders(page,limit,longitude,latitude));
    }
    @GetMapping("/saveAll")
    public Result saveAll()
    {
       ;
        return Result.ok( leaderService.saveAllLeaderEs());
    }
}
