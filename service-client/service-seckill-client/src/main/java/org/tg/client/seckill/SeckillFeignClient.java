package org.tg.client.seckill;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.tg.vo.order.OrderSubmitVo;
import org.tg.vo.product.SkuStockLockVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-15 20:37:36
 * @Description
 * @Version 1.0
 */
@FeignClient(contextId = "seckill",name = "service-activity")
public interface SeckillFeignClient {
    @GetMapping("/api/seckill/findHomeData")
    Map<String, Object> findHomeData();
    @GetMapping("/api/seckill/checkAndMinusStock/{orderNo}")
    Boolean checkAndMinusStock(
            @RequestBody  List<SkuStockLockVo> seckillStockLockVoList,
                         @PathVariable("orderNo") String orderNo);
}
