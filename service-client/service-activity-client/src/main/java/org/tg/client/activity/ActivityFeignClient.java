package org.tg.client.activity;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.tg.model.activity.CouponInfo;
import org.tg.model.activity.SeckillSku;
import org.tg.model.order.CartInfo;
import org.tg.vo.activity.SeckillSkuVo;
import org.tg.vo.order.CartInfoVo;
import org.tg.vo.order.OrderConfirmVo;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2023-07-15 15:53:42
 * @Description
 * @Version 1.0
 */
@FeignClient(contextId ="activity",name = "service-activity")
public interface ActivityFeignClient {


    @GetMapping("/api/seckill/getSeckillSkuVo/{id}")
    SeckillSkuVo getSeckillSkuVo(@PathVariable("id") long id);
    @PostMapping("/api/activity/inner/findActivity")
    Map<Long, List<String>> findActivity(@RequestBody List<Long> skuIdList);
    @ApiOperation(value = "根据skuId、userId获取促销信息和优惠券信息")
    @PostMapping("/api/activity/inner/findActivityAndCoupon/{skuId}/{userId}")
    public Map<String, Object>  findActivityAndCoupon(
            @PathVariable("skuId") Long skuId,
            @PathVariable("userId")Long userId
    );
    @PostMapping("/api/activity/inner/findCartActivityAndCoupon/{userId}")
    OrderConfirmVo findCartActivityAndCoupon(@RequestBody List<CartInfo> cartInfoList, @PathVariable("userId") Long userId);

    @GetMapping(value = "/api/activity/inner/updateCouponInfoUsedTime/{couponId}/{userId}")
    Boolean updateCouponInfoUsedTime(@PathVariable("couponId") Long couponId, @PathVariable("userId") Long userId);
    @GetMapping(value = "/api/activity/inner/updateCouponInfoUseStatus/{couponId}/{userId}/{orderId}")
    Boolean updateCouponInfoUseStatus(@PathVariable("couponId") Long couponId, @PathVariable("userId") Long userId, @PathVariable("orderId") Long orderId);
    @PostMapping(value = "/api/activity/inner/findCartActivityList")
    List<CartInfoVo> findCartActivityList(@RequestBody List<CartInfo> cartInfoList);
    @PostMapping(value = "/api/activity/inner/findRangeSkuIdList/{couponId}")
    CouponInfo findRangeSkuIdList(@RequestBody List<CartInfo> cartInfoList, @PathVariable("couponId") Long couponId);

    @ApiOperation(value = "根据活动id获取活动skuid列表")
    @GetMapping(value = "/api/activity/inner/findSkuIdList/{activityId}")
    List<Long> findSkuIdList(@PathVariable("activityId") Long activityId);

    @GetMapping("/getSeckillSkuBySkuId/{skuId}")
    SeckillSku getSeckillSkuBySkuId(@PathVariable("skuId") Integer skuId);
}
