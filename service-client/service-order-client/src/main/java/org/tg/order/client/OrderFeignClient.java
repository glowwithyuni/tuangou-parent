package org.tg.order.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.tg.model.order.OrderInfo;

/**
 * @Author Glow
 * @Date 2023-08-01 10:27:10
 * @Description
 * @Version 1.0
 */
@FeignClient("service-order")
public interface OrderFeignClient {

    @GetMapping("/api/order/inner/getOrderInfo/{orderNo}")
    public OrderInfo getOrderInfo(@PathVariable("orderNo") String orderNo);

}
