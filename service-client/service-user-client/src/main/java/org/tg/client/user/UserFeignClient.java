package org.tg.client.user;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.tg.common.result.Result;
import org.tg.model.user.Leader;
import org.tg.model.user.UserDelivery;
import org.tg.vo.user.LeaderAddressVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-15 15:39:37
 * @Description
 * @Version 1.0
 */
@FeignClient(value = "service-user")
public interface UserFeignClient {

    @ApiOperation("根据Id获取提货仓库信息")
    @GetMapping("/api/user/leader/getUserDeliveryByUserIdAndLeaderId/{leaderId}/{userId}")
    public UserDelivery getUserDeliveryByUserIdAndLeaderId(
            @PathVariable("userId") Long userId, @PathVariable("leaderId") Long leaderId);

    @GetMapping("/api/user/leader/getLeaderById/{leaderId}")
    public Leader getLeaderById(@PathVariable("leaderId") Long leaderId);

    @GetMapping("/api/user/leader/inner/getUserAddressByUserId/{userId}")
    LeaderAddressVo getLeaderAddressVoByUserId(@PathVariable(value = "userId") Long userId);
    @GetMapping("/api/user/leader/inner/getLeaderIdByUserId/{userId}")
    public Long getLeaderIdByUserId(@PathVariable(value = "userId") Long userId);
    @GetMapping("/admin/user/leader/list/{page}/{limit}")
    public Result pagelist(@PathVariable Long page, @PathVariable Long limit);
    @GetMapping("/admin/user/leader/list")
    public List<Leader> list();
}
