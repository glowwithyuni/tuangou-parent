package org.tg.client.sys;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.tg.common.result.Result;
import org.tg.model.sys.Region;

/**
 * @Author Glow
 * @Date 2023-07-12 14:46:09
 * @Description
 * @Version 1.0
 */
@FeignClient(value = "service-sys")
public interface RegionFeignClient {
    @ApiOperation(value = "根据根据地域id获取仓库Id")
    @GetMapping("api/sys/inner/getWareIdByRegionId/{regionId}")
    public Long getWareId(@PathVariable Long regionId);
    @GetMapping("/admin/sys/region/findByParentId/{parentId}")
    public Result findByParentId(@PathVariable("parentId") Long parentId);
    @GetMapping("/admin/sys/region/getRegionById/{id}")
    Region getRegionById(@PathVariable("id") Long id);
}
