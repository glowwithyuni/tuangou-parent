package org.tg.client.product;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tg.common.result.Result;
import org.tg.model.product.Category;
import org.tg.model.product.SkuInfo;
import org.tg.vo.product.SkuInfoVo;
import org.tg.vo.product.SkuStockLockVo;

import java.awt.*;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-04 14:39:57
 * @Description 远程调用商品服务
 * @Version 1.0
 */
@FeignClient(value = "service-product")
public interface ProductFeignClient {

    //根据skuId获取sku信息
    @GetMapping("/api/product/inner/getSkuInfoVo/{skuId}")
    public SkuInfoVo getSkuInfoVo(@PathVariable Long skuId);
    @GetMapping("/api/product/inner/getSkuListByCategoryId/{categoryId}")
    public List<SkuInfo> getSkuListByCategoryId(@PathVariable("categoryId") Long categoryId);
    @PostMapping(value = "/api/product/inner/fileUpload",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Result fileUpload(@RequestPart(value = "file")MultipartFile file) throws Exception;
    /**
     * 获取分类信息
     * @return
     */
    @GetMapping("/api/product/inner/findAllCategoryList")
    List<Category> findAllCategoryList();

    /**
     * 获取新人专享
     * @return
     */
    @GetMapping("/api/product/inner/findNewPersonSkuInfoList")
    List<SkuInfo> findNewPersonSkuInfoList();
    /**
     * 批量获取分类信息
     * @param categoryIdList
     * @return
     */
    @PostMapping("/api/product/inner/findCategoryList")
    List<Category> findCategoryList(@RequestBody List<Long> categoryIdList);
    @GetMapping("/api/product/inner/getCategory/{categoryId}")
    public Category getCategory(@PathVariable("categoryId") Long categoryId);

    @GetMapping("/api/product/inner/getSkuInfo/{skuId}")
    public SkuInfo getSkuInfo(@PathVariable("skuId") Long skuId);
    /**
     * 批量获取sku信息
     * @param skuIdList
     * @return
     */
    @PostMapping("/api/product/inner/findSkuInfoList")
    List<SkuInfo> findSkuInfoList(@RequestBody List<Long> skuIdList);

    /**
     * 根据关键字获取sku列表，活动使用
     * @param keyword
     * @return
     */
    @GetMapping("/api/product/inner/findSkuInfoByKeyword/{keyword}")
    List<SkuInfo> findSkuInfoByKeyword(@PathVariable("keyword") String keyword);
    @PostMapping("/api/product/inner/updateSkuType/{skuType}")
    int updateSkuType(@RequestBody List<Long> skuIdLists, @PathVariable("skuType") Integer skuType);
    //验证和锁定库存
    @PostMapping("/api/product/inner/checkAndLock/{orderNo}")
    public Boolean checkAndLock(@RequestBody List<SkuStockLockVo> skuStockLockVoList,
                                @PathVariable("orderNo") String orderNo);
}