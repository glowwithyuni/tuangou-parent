package org.tg.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.tg.model.search.SkuEs;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-15 15:31:16
 * @Description
 * @Version 1.0
 */
@FeignClient(value = "service-search")
public interface SearchFeignClient {
    /**
     * 获取热门商品列表
     * @return
     */
    @GetMapping("api/search/sku/inner/findHotSkuList")
    List<SkuEs> findHotSkuList();
    //更新商品热度
    @GetMapping("/api/search/sku/inner/incrHotScore/{skuId}")
    public Boolean incrHotScore(@PathVariable("skuId") Long skuId);
}
