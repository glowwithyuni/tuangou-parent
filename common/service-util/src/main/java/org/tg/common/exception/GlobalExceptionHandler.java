package org.tg.common.exception;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tg.common.result.Result;
import org.tg.common.result.ResultCodeEnum;

/**
 * @Author Glow
 * @Date 2023-06-23 13:08:31
 * @Description 全局统一异常类
 * @Version 1.0
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e){
        e.printStackTrace();
        return Result.fail(e.getMessage());
    }
    @ExceptionHandler(CustomExceptionHandler.class)
    @ResponseBody
    public Result error(CustomExceptionHandler e)
    {

         return Result.build(null,e.getCode(),e.getMessage());
    }
}