package org.tg.common.auth;


import org.tg.vo.acl.AdminLoginVo;
import org.tg.vo.user.UserLoginVo;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-13 15:18:28
 * @Description 用户ThreadLocal变量工具类
 * @Version 1.0
 */
public class AuthContextHolder {

    //小程序用户id
    private static ThreadLocal<Long> userId = new ThreadLocal<>();
    //管理端用户ID
    private static ThreadLocal<Long> adminId = new ThreadLocal<>();
    //用户仓库id
    private static ThreadLocal<Long> wareId = new ThreadLocal<>();
    //管理端用户权限ID
    private static ThreadLocal<List<Long>> roleId = new ThreadLocal<>();
    //用户信息对象
    private static ThreadLocal<UserLoginVo> userLoginVo = new ThreadLocal<>();
    //管理端信息对象
    private static ThreadLocal<AdminLoginVo> adminLoginVo = new ThreadLocal<>();
    //userId操作的方法
    public static void setUserId(Long _userId) {
        userId.set(_userId);
    }

    public static Long getUserId() {
        return userId.get();
    }
    public static void setAdminId(Long _adminId) {
        adminId.set(_adminId);
    }

    public static Long getAdminId() {
        return adminId.get();
    }

    public static Long getWareId(){
        return wareId.get();
    }

    public static void setWareId(Long _wareId){
        wareId.set(_wareId);
    }

    public static List<Long> getRoleId(){
        return roleId.get();
    }

    public static void setRoleId(List<Long> _roleId){
        roleId.set(_roleId);
    }
    public static UserLoginVo getUserLoginVo() {
        return userLoginVo.get();
    }

    public static void setUserLoginVo(UserLoginVo _userLoginVo) {
        userLoginVo.set(_userLoginVo);
    }


    public static AdminLoginVo getAdminLoginVo() {
        return adminLoginVo.get();
    }

    public static void setAdminLoginVo(AdminLoginVo _adminLoginVo) {
        adminLoginVo.set(_adminLoginVo);
    }


}

