package org.tg.common.result;


import com.baomidou.mybatisplus.extension.api.R;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 12:49:36
 * @Description 统一信息结果类
 * @Version 1.0
 */
@Data
public class Result<T> {
//    code ,msg,data模型
    private Integer code;
    private String msg;
    private T data;
    private Result() { }
    //设置数据，返回对象
    public static<T> Result<T>  build(T data,ResultCodeEnum resultCodeEnum)
    {
        Result<T> result = new Result<>();
        if(data != null)
        {
            result.setData(data);
        }
        result.setCode(resultCodeEnum.getCode());
        result.setMsg(resultCodeEnum.getMessage());
        return result;
    }
    //设置数据，返回对象
    public static<T> Result<T>  build(T data,Integer code,String msg)
    {
        Result<T> result = new Result<>();
        if(data != null)
        {
            result.setData(data);
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    //200成功
    public static<T> Result<T> ok(T data)
    {
        Result<T> result = build(data, ResultCodeEnum.SUCCESS);
        return result;
    }
    //200成功
    public static<T> Result<T> ok()
    {
        Result<T> result = build(null, ResultCodeEnum.SUCCESS);
        return result;
    }
    //201失败
    public static<T> Result<T> fail(T data) {
        return build(data,ResultCodeEnum.FAIL);
    }
    //201失败
    public static<T> Result<T> fail() {
        return build(null,ResultCodeEnum.FAIL);
    }

}
