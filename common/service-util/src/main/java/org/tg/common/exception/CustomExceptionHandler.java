package org.tg.common.exception;


import lombok.Data;
import org.tg.common.result.ResultCodeEnum;

/**
 * @Author Glow
 * @Date 2023-06-23 13:11:00
 * @Description 自定义异常处理类
 * @Version 1.0
 */
@Data
public class CustomExceptionHandler extends RuntimeException {
    private Integer code;
    public CustomExceptionHandler(String message,Integer code)
    {
        super(message);
        this.code = code;
    }
    public CustomExceptionHandler(ResultCodeEnum resultCodeEnum)
    {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }
    @Override
    public String toString() {
        return "TgException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
