package org.tg.common.auth;


import jodd.util.CollectionUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.tg.common.constant.RedisConst;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author Glow
 * @Date 2023-07-13 15:21:41
 * @Description
 * @Version 1.0
 */
@Configuration
public class LoginMvcConfigurerAdapter extends WebMvcConfigurationSupport {
   //在WebMvcConfigurationSupport继承类添加资源路径添加即可解决Swagger配置覆盖缺失。
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Resource
    private RedisTemplate redisTemplate;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       List<String> list =  (List<String>)redisTemplate.opsForValue().get
                (RedisConst.ADMIN_ROLE_PATH_PREFIX+AuthContextHolder.getAdminId());
            if(!CollectionUtils.isEmpty(list))
            {
                registry.addInterceptor(new UserLoginInterceptor(redisTemplate))
                        .addPathPatterns("/api/**")
                        .addPathPatterns("/admin/**")
                        .excludePathPatterns("/api/user/weixin/wxLogin/*")
                        .excludePathPatterns("/admin/acl/index/login/*")
                        .excludePathPatterns("/admin/acl/index/logout/*")
                        .excludePathPatterns("/admin/acl/index/info")
                        .excludePathPatterns( list);
            }
            else {
                registry.addInterceptor(new UserLoginInterceptor(redisTemplate))
                        .addPathPatterns("/api/**")
                        .addPathPatterns("/admin/**")
                        .excludePathPatterns("/api/user/weixin/wxLogin/*")
                        .excludePathPatterns("/admin/acl/index/login/*")
                ;
            }




        super.addInterceptors(registry);
    }
}

