package org.tg.common.util.helper;


import io.jsonwebtoken.*;
import org.apache.commons.lang.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-07-11 14:46:34
 * @Description JWT工具类
 * @Version 1.0
 */
public class JwtHelper {
    private static long tokenExpiration = 365*24*60*60*1000;
    private static String tokenSignKey = "tuangou";
    private static String tokenAdminSignkey = "tuangouAdmin";
    public static String createToken(Long userId, String userName) {
        String token = Jwts.builder()
                .setSubject("tuangou-USER")
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .claim("userId", userId)
                .claim("userName", userName)
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)
                .compressWith(CompressionCodecs.GZIP)
                .compact();
        return token;
    }
    public static String createAdminToken(Long userId,Long wareId) {
        String token = Jwts.builder()
                .setSubject("tuangou-admin")
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .claim("userId", userId)
//                .claim("userName", userName)
                .claim("wareId",wareId)
                .signWith(SignatureAlgorithm.HS512, tokenAdminSignkey)
                .compressWith(CompressionCodecs.GZIP)
                .compact();
        return token;
    }
    public static Long getUserId(String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        Integer userId = (Integer)claims.get("userId");
        return userId.longValue();
        // return 1L;
    }

    public static String getUserName(String token) {
        if(StringUtils.isEmpty(token)) {
            return "";
        }

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return (String)claims.get("userName");
    }

    //管理端token管理
    public static Long getAdminUserId(String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenAdminSignkey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        Integer userId = (Integer)claims.get("userId");
        return userId.longValue();
        // return 1L;
    }
//    public static String getAdminUserName(String token) {
//        if(StringUtils.isEmpty(token)) {
//            return "";
//        }
//
//        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenAdminSignkey).parseClaimsJws(token);
//        Claims claims = claimsJws.getBody();
//        return (String)claims.get("userName");
//    }
    public static Long getAdminwareId(String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenAdminSignkey).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return Long.valueOf(String.valueOf(claims.get("wareId")));
    }
    public static void removeToken(String token) {
        //jwttoken无需删除，客户端扔掉即可。
    }

    public static void main(String[] args) {
        String token = JwtHelper.createToken(7L, "admin");
        System.out.println(token);
        System.out.println(JwtHelper.getUserId(token));
        System.out.println(JwtHelper.getUserName(token));
    }
}
