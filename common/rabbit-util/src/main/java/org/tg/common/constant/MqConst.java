package org.tg.common.constant;


/**
 * @Author Glow
 * @Date 2023-07-04 22:50:59
 * @Description MQ常量配置
 * @Version 1.0
 */
public class MqConst {
    /**
     * 消息补偿
     */
    public static final String MQ_KEY_PREFIX = "tuangou.mq:list";
    public static final int RETRY_COUNT = 3;

    /**
     * 商品上下架
     */
    public static final String EXCHANGE_GOODS_DIRECT = "tuangou.goods.direct";
    public static final String ROUTING_GOODS_UPPER = "tuangou.goods.upper";
    public static final String ROUTING_GOODS_LOWER = "tuangou.goods.lower";
    //队列
    public static final String QUEUE_GOODS_UPPER  = "tuangou.goods.upper";
    public static final String QUEUE_GOODS_LOWER  = "tuangou.goods.lower";

    /**
     * 团长上下线
     */
    public static final String EXCHANGE_LEADER_DIRECT = "tuangou.leader.direct";
    public static final String ROUTING_LEADER_UPPER = "tuangou.leader.upper";
    public static final String ROUTING_LEADER_LOWER = "tuangou.leader.lower";
    //队列
    public static final String QUEUE_LEADER_UPPER  = "tuangou.leader.upper";
    public static final String QUEUE_LEADER_LOWER  = "tuangou.leader.lower";

    //订单
    public static final String EXCHANGE_ORDER_DIRECT = "tuangou.order.direct";

    public static final String ROUTING_ROLLBACK_STOCK = "tuangou.rollback.stock";
    public static final String ROUTING_MINUS_STOCK = "tuangou.minus.stock";
    public static final String ROUTING_DELETE_CART = "tuangou.delete.cart";
    //解锁普通商品库存
    public static final String QUEUE_ROLLBACK_STOCK = "tuangou.rollback.stock";
    public static final String QUEUE_SECKILL_ROLLBACK_STOCK = "tuangou.seckill.rollback.stock";
    public static final String QUEUE_MINUS_STOCK = "tuangou.minus.stock";
    public static final String QUEUE_DELETE_CART = "tuangou.delete.cart";

    //支付
    public static final String EXCHANGE_PAY_DIRECT = "tuangou.pay.direct";
    public static final String ROUTING_PAY_SUCCESS = "tuangou.pay.success";
    public static final String QUEUE_ORDER_PAY  = "tuangou.order.pay";
    public static final String QUEUE_LEADER_BILL  = "tuangou.leader.bill";

    //取消订单
    public static final String EXCHANGE_CANCEL_ORDER_DIRECT = "tuangou.cancel.order.direct";
    public static final String ROUTING_CANCEL_ORDER = "tuangou.cancel.order";
    //延迟取消订单队列
    public static final String QUEUE_CANCEL_ORDER  = "tuangou.cancel.order";

    /**
     * 定时任务
     */
    public static final String EXCHANGE_DIRECT_TASK = "tuangou.exchange.direct.task";
    public static final String ROUTING_TASK_23 = "tuangou.task.23";
    //队列
    public static final String QUEUE_TASK_23  = "tuangou.queue.task.23";
}

