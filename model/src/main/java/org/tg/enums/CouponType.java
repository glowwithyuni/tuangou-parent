package org.tg.enums;
import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * @Author Glow
 * @Date 2023-06-23 14:37:34
 * @Description 打折类型枚举
 * @Version 1.0
 */

@Getter
public enum CouponType {
    FULL_REDUCTION(1,"满减"),
    CASH(2,"现金卷");

    @EnumValue
    private Integer code;
    private String comment ;

    CouponType(Integer code, String comment ){
        this.code=code;
        this.comment=comment;
    }
}