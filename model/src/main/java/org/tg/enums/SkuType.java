package org.tg.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;
/**
 * @Author Glow
 * @Date 2023-06-23 14:45:25
 * @Description 商品规格
 * @Version 1.0
 */
@Getter
public enum SkuType {
    COMMON(0,"普通"),
    SECKILL(1,"秒杀" );

    @EnumValue
    private Integer code ;
    private String comment ;

    SkuType(Integer code, String comment ){
        this.code=code;
        this.comment=comment;
    }
}