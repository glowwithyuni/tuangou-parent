package org.tg.model.activity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 15:09:47
 * @Description 活动促销产品
 * @Version 1.0
 */
@Data
@ApiModel(description = "ActivitySku（活动促销产品）")
@TableName("activity_sku")
public class ActivitySku extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "活动id ")
    @TableField("activity_id")
    private Long activityId;

    @ApiModelProperty(value = "sku_id")
    @TableField("sku_id")
    private Long skuId;

//	@TableField(exist = false)
//	private SkuInfo skuInfo;

}