package org.tg.model.activity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

import java.util.Date;

/**
 * @Author Glow
 * @Date 2023-06-23 15:24:28
 * @Description 秒杀活动商品提醒
 * @Version 1.0
 */
@Data
@ApiModel(description = "SeckillSkuNotice（秒杀活动商品提醒）")
@TableName("seckill_sku_notice")
public class SeckillSkuNotice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "user_id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty(value = "sku_id")
    @TableField("sku_id")
    private Long skuId;

    @ApiModelProperty(value = "活动场次id")
    @TableField("session_id")
    private Long sessionId;

    @ApiModelProperty(value = "订阅时间")
    @TableField("subcribe_time")
    private Date subcribeTime;

    @ApiModelProperty(value = "发送时间")
    @TableField("send_time")
    private Date sendTime;

    @ApiModelProperty(value = "通知方式[0-短信，1-邮件]")
    @TableField("notice_type")
    private Boolean noticeType;

}
