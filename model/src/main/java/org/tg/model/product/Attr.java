package org.tg.model.product;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 16:44:42
 * @Description 商品属性、规格
 * @Version 1.0
 */
@Data
@ApiModel(description = "Attr（ 商品属性、规格）")
@TableName("attr")
public class Attr extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "属性名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "属性录入方式：0->手工录入；1->从列表中选取")
    @TableField("input_type")
    private Integer inputType;

    @ApiModelProperty(value = "可选值列表[用逗号分隔]")
    @TableField("select_list")
    private String selectList;

    @ApiModelProperty(value = "属性分组id")
    @TableField("attr_group_id")
    private Long attrGroupId;

}