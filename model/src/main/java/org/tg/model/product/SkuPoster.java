package org.tg.model.product;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 17:00:32
 * @Description 不同规格的商品上传
 * @Version 1.0
 */
@Data
@ApiModel(description = "SkuPoster（不同规格的商品上传）")
@TableName("sku_poster")
public class SkuPoster extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品id")
    @TableField("sku_id")
    private Long skuId;

    @ApiModelProperty(value = "文件名称")
    @TableField("img_name")
    private String imgName;

    @ApiModelProperty(value = "文件路径")
    @TableField("img_url")
    private String imgUrl;

}
