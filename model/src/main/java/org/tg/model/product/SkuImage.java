package org.tg.model.product;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 16:58:26
 * @Description 不同规格的商品的图片
 * @Version 1.0
 */
@Data
@ApiModel(description = "SkuImages（不同规格的商品的图片）")
@TableName("sku_image")
public class SkuImage extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "sku_id")
    @TableField("sku_id")
    private Long skuId;

    @ApiModelProperty(value = "图片名称")
    @TableField("img_name")
    private String imgName;

    @ApiModelProperty(value = "图片地址")
    @TableField("img_url")
    private String imgUrl;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;

}