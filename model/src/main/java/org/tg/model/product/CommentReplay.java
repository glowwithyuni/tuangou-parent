package org.tg.model.product;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 16:47:17
 * @Description 精选评论
 * @Version 1.0
 */
@Data
@ApiModel(description = "CommentReplay（精选评论）")
@TableName("comment_replay")
public class CommentReplay extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "commentId")
    @TableField("comment_id")
    private Long commentId;

    @ApiModelProperty(value = "nickName")
    @TableField("nick_name")
    private String nickName;

    @ApiModelProperty(value = "icon")
    @TableField("icon")
    private String icon;

    @ApiModelProperty(value = "content")
    @TableField("content")
    private String content;

}
