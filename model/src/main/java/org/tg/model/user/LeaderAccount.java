package org.tg.model.user;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

import java.math.BigDecimal;

/**
 * @Author Glow
 * @Date 2023-06-23 17:59:38
 * @Description 团长账户信息
 * @Version 1.0
 */
@Data
@ApiModel(description = "LeaderAccount（团长账户信息）")
@TableName("leader_account")
public class LeaderAccount extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "团长ID")
    @TableField("leader_id")
    private Long leaderId;

    @ApiModelProperty(value = "总收益, 可能有部分余额因为订单未结束而不能提现")
    @TableField("total_amount")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "可提现余额")
    @TableField("available_amount")
    private BigDecimal availableAmount;

    @ApiModelProperty(value = "冻结余额")
    @TableField("frozen_amount")
    private BigDecimal frozenAmount;

}