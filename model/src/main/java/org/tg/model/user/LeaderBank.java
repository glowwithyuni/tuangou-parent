package org.tg.model.user;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 18:00:28
 * @Description 团购团长绑定的支付渠道
 * @Version 1.0
 */
@Data
@ApiModel(description = "LeaderBank（团购团长绑定的支付渠道）")
@TableName("leader_bank")
public class LeaderBank extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "团长ID")
    @TableField("leader_id")
    private String leaderId;

    @ApiModelProperty(value = "账户类型(微信,银行)")
    @TableField("account_type")
    private String accountType;

    @ApiModelProperty(value = "银行名称")
    @TableField("bank_name")
    private String bankName;

    @ApiModelProperty(value = "银行账号")
    @TableField("bank_account_no")
    private String bankAccountNo;

    @ApiModelProperty(value = "银行账户名")
    @TableField("bank_account_name")
    private String bankAccountName;

    @ApiModelProperty(value = "微信ID")
    @TableField("wechat_id")
    private String wechatId;

}