package org.tg.model.user;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 18:23:20
 * @Description 用户登录日志记录
 * @Version 1.0
 */
@Data
@ApiModel(description = "UserLoginLog（用户登录日志记录）")
@TableName("user_login_log")
public class UserLoginLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty(value = "登录ip")
    @TableField("ip")
    private String ip;

    @ApiModelProperty(value = "登录城市")
    @TableField("city")
    private String city;

    @ApiModelProperty(value = "登录类型【0-web，1-移动】")
    @TableField("type")
    private Boolean type;

}