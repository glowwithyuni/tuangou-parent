package org.tg.model.user;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 18:02:54
 * @Description 跟团的用户
 * @Version 1.0
 */
@Data
@ApiModel(description = "LeaderUser（跟团的用户）")
@TableName("leader_user")
public class LeaderUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "团长ID")
    @TableField("leader_id")
    private Long leaderId;

    @ApiModelProperty(value = "userId")
    @TableField("user_id")
    private Long userId;

}