package org.tg.model.order;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.base.BaseEntity;

/**
 * @Author Glow
 * @Date 2023-06-23 16:28:46
 * @Description 订单退货
 * @Version 1.0
 */

@Data
@ApiModel(description = "OrderReturnReason（订单退货）")
@TableName("order_return_reason")
public class OrderReturnReason extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "退货类型")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "sort")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "状态：0->不启用；1->启用")
    @TableField("status")
    private Integer status;

}