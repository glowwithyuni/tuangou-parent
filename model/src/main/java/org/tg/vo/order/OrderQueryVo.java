package org.tg.vo.order;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.enums.OrderStatus;

/**
 * @Author Glow
 * @Date 2023-06-23 21:37:03
 * @Description 订单查询vo
 * @Version 1.0
 */
@Data
public class OrderQueryVo {


    @ApiModelProperty(value = "订单号")
    private String outTradeNo
            ;

    @ApiModelProperty(value = "收货人信息")
    private String receiver;

    @ApiModelProperty(value = "订单状态")
    private OrderStatus orderStatus;

    @ApiModelProperty(value = "团长id")
    private Long leaderId;

    @ApiModelProperty(value = "仓库id")
    private Long wareId;

    @ApiModelProperty(value = "创建时间")
    private String createTimeBegin;
    private String createTimeEnd;

}
