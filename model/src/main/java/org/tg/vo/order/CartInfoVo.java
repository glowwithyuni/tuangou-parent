package org.tg.vo.order;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.activity.ActivityRule;
import org.tg.model.order.CartInfo;

import java.io.Serializable;
import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-23 21:26:46
 * @Description 购物车vo
 * @Version 1.0
 */
@Data
public class CartInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 购物项凑单，同一活动对应的最优活动规则
     */
    @ApiModelProperty(value = "cartInfoList")
    private List<CartInfo> cartInfoList;

    @ApiModelProperty(value = "活动规则")
    private ActivityRule activityRule;


}

