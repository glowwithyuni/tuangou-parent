package org.tg.vo.order;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.enums.OrderStatus;

/**
 * @Author Glow
 * @Date 2023-06-23 21:38:45
 * @Description 用户订单状态vo
 * @Version 1.0
 */
@Data
public class OrderUserQueryVo {

    private Long userId;

    @ApiModelProperty(value = "订单状态")
    private OrderStatus orderStatus;

}
