package org.tg.vo.order;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.enums.SkuType;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-06-23 21:28:59
 * @Description 商品订单详情MQvo
 * @Version 1.0
 */
@Data
@ApiModel(description = "OrderItem(订单商品编号及其购买数量MQvo)")
public class OrderItemMqVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品类型：0->普通商品 1->秒杀商品")
    private SkuType skuType;

    @ApiModelProperty(value = "商品sku编号")
    private Long skuId;

    @ApiModelProperty(value = "商品购买的数量")
    private Integer skuNum;

}