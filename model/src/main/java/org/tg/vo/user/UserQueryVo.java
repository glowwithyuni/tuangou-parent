package org.tg.vo.user;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:54:29
 * @Description 用户个人信息查询vo
 * @Version 1.0
 */
@Data
public class UserQueryVo {

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "身份证号码")
    private String idNo;

    @ApiModelProperty(value = "性别")
    private String sex;

    @ApiModelProperty(value = "电话号码")
    private String phone;

}