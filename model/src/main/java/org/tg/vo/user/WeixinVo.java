package org.tg.vo.user;


import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-06-23 21:55:40
 * @Description 微信Vo
 * @Version 1.0
 */
@Data
@ApiModel(description = "微信Vo")
public class WeixinVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String iv;
    private String encryptedData;
    private String sessionKey;
    private String openId;

}