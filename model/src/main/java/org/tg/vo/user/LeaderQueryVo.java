package org.tg.vo.user;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:52:37
 * @Description 团购团长状态更改查询vo
 * @Version 1.0
 */
@Data
public class LeaderQueryVo {

    @ApiModelProperty(value = "审核状态")
    private Integer checkStatus;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "省")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区域")
    private String district;

}
