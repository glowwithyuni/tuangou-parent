package org.tg.vo.search;


import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:46:21
 * @Description 封装团购地理位置vo
 * @Version 1.0
 */
@Data
public class LeaderEsQueryVo {

    double latitude = 39.9504550;
    double longitude = 116.3512330;
    double distance = 100;

}