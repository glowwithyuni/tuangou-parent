package org.tg.vo.search;


import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:46:56
 * @Description 商品es分类关键字查询
 * @Version 1.0
 */

@Data
public class SkuEsQueryVo {

    private Long categoryId;;//三级分类id

    private String keyword;//检索的关键字

    private Long wareId;

}