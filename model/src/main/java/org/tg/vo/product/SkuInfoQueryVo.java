package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:43:14
 * @Description 不同规格的商品查询vo
 * @Version 1.0
 */
@Data
public class SkuInfoQueryVo {

    @ApiModelProperty(value = "分类id")
    private Long categoryId;

    @ApiModelProperty(value = "商品类型：0->普通商品 1->秒杀商品")
    private String skuType;

    @ApiModelProperty(value = "spu名称")
    private String keyword;

}