package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:40:58
 * @Description 商品属性组名
 * @Version 1.0
 */
@Data
public class AttrGroupQueryVo {

    @ApiModelProperty(value = "组名")
    private String name;

}

