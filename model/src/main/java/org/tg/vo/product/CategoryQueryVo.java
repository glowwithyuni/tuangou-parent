package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:41:57
 * @Description 商品分类名查询vo
 * @Version 1.0
 */
@Data
public class CategoryQueryVo {

    @ApiModelProperty(value = "分类名称")
    private String name;

}