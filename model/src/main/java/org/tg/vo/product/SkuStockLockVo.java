package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-06-23 21:44:43
 * @Description 商品锁定vo
 * @Version 1.0
 */
@Data
public class SkuStockLockVo implements Serializable {

    @ApiModelProperty(value = "skuId")
    private Long skuId;

    @ApiModelProperty(value = "sku个数")
    private Integer skuNum;

    @ApiModelProperty(value = "是否锁定")
    private Boolean isLock = false;
}
