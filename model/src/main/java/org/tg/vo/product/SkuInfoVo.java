package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.tg.model.product.SkuAttrValue;
import org.tg.model.product.SkuImage;
import org.tg.model.product.SkuInfo;
import org.tg.model.product.SkuPoster;

import java.util.List;

/**
 * @Author Glow
 * @Date 2023-06-23 21:44:08
 * @Description 不同规格商品详细信息vo
 * @Version 1.0
 */
@Data
public class SkuInfoVo extends SkuInfo {

    @ApiModelProperty(value = "海报列表")
    private List<SkuPoster> skuPosterList;

    @ApiModelProperty(value = "属性值")
    private List<SkuAttrValue> skuAttrValueList;

    @ApiModelProperty(value = "图片")
    private List<SkuImage> skuImagesList;

}
