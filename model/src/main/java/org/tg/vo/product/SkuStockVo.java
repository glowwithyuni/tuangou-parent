package org.tg.vo.product;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-06-23 21:45:10
 * @Description 商品库存vo
 * @Version 1.0
 */
@Data
public class SkuStockVo implements Serializable {

    @ApiModelProperty(value = "skuId")
    private Long skuId;

    @ApiModelProperty(value = "sku类型")
    private Integer skuType;

    @ApiModelProperty(value = "更新的库存数量")
    private Integer stockNum;

}