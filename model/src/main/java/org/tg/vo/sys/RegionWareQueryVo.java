package org.tg.vo.sys;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:48:20
 * @Description 区域仓库vo
 * @Version 1.0
 */

@Data
public class RegionWareQueryVo {

    @ApiModelProperty(value = "关键字")
    private String keyword;

}

