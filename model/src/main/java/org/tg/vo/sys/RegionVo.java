package org.tg.vo.sys;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:47:55
 * @Description 区域vo
 * @Version 1.0
 */
@Data
public class RegionVo {

    @ApiModelProperty(value = "开通区域")
    private Long regionId;

    @ApiModelProperty(value = "区域名称")
    private String regionName;

}
