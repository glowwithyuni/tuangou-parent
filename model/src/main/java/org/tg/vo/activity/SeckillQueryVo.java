package org.tg.vo.activity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 18:38:22
 * @Description 秒杀活动状态查询vo
 * @Version 1.0
 */
@Data
public class SeckillQueryVo {

    @ApiModelProperty(value = "活动标题")
    private String title;

    @ApiModelProperty(value = "上下线状态")
    private Integer status;


}
