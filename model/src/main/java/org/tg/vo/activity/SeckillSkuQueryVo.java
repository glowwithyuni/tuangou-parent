package org.tg.vo.activity;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Glow
 * @Date 2023-06-23 21:25:13
 * @Description 秒杀商品活动场次查询vo
 * @Version 1.0
 */
@Data
public class SeckillSkuQueryVo {

    @ApiModelProperty(value = "秒杀活动id")
    private Long seckillId;

    @ApiModelProperty(value = "活动场次id")
    private Long seckillTimeId;

}