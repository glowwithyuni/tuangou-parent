package org.tg.vo.acl;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author Glow
 * @Date 2023-06-23 18:35:27
 * @Description
 * @Version 1.0
 */
@Data
@ApiModel(description = "角色查询实体")
public class RoleQueryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

}