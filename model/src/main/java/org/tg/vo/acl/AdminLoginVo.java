package org.tg.vo.acl;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @Author Glow
 * @Date 2023-06-23 18:33:09
 * @Description 管理员登录信息
 * @Version 1.0
 */
@Data
@ApiModel(description = "管理员登录信息")
public class AdminLoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "管理员id")
    private Long adminId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "仓库id")
    private Long wareId;
    @ApiModelProperty(value = "权限id")
    private List<Long> roleId;
    @ApiModelProperty(value = "权限路径")
    private List<String> path;

}